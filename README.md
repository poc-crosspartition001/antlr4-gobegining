# Assorted Golang ANTLR examples
This is an Example of ANTLR golang code.

## Objectives.
1. Confirm system setup
2. Test and Learn how a gremlin query is Traversed including the gremlin Methods
3. A means of understanding the order of ANTLR method calls
4. Testing WHICH Methods are called.


## NOTES
I have found this code very effective in studying which methods are called on a particular query.
The Example Queries are located in /GremFiles.

# HOWTO
run *** /v01/parser/remoteconnectionparser_test.go ***


### GremFiles
Example Gremlin queries are maintained in GremFiles directory in separate files.
Bad practice going forward as the correct way would be using testable but its easier to 
observe in separate files.

### GREMLIN GRAMMER
The files are inherited from go SDK


### setup

Make sure globalprotect vm is on
and...
export GOPRIVATE="bitbucket.org/cloudcoreo"
export GONOSUMDB="bitbucket.org/cloudcoreo"


# TODO  this part
1. go get github.com/antlr/antlr4/runtime/Go/antlr


2.  go get bitbucket.org/cloudcoreo/go-services-sdk.git
update

"bitbucket.org/cloudcoreo/go-services-sdk/gremlin-grammar/gremlin"

