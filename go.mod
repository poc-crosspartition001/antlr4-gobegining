module antlr4-gobegining

go 1.16

require (
	bitbucket.org/cloudcoreo/go-services-sdk v0.0.0-20220211192518-b640ac2b65cb
	bitbucket.org/cloudcoreo/vss-inventory-service v0.0.0-20220210193959-2dfaef58f7f9
	github.com/antlr/antlr4/runtime/Go/antlr v0.0.0-20210707120613-a80295a60b3e
)
