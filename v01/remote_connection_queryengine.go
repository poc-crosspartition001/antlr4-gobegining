package v01

import (
	"bitbucket.org/cloudcoreo/go-services-sdk/log"
	"bitbucket.org/cloudcoreo/vss-inventory-service/storage/models"
	"context"
	"errors"
)

const (
	VERSION_NUMBER = "v1"
	NAME           = "remote-connection-query-engine"
)

var (
	ErrEngineNotImplemented = errors.New("query engine not implemented")
)

type RemoteConnectionQueryEngine struct {
	logger *log.Logger
}

func Initialize(logger *log.Logger) (*RemoteConnectionQueryEngine, error) {
	return &RemoteConnectionQueryEngine{
		logger: logger,
	}, nil
}

func (nqe *RemoteConnectionQueryEngine) Execute(ctx context.Context, orgID string, request *models.GraphQueryRequest) (*models.GraphQueryResponse, error) {
	return nil, ErrEngineNotImplemented
}

func (nqe *RemoteConnectionQueryEngine) Validate(ctx context.Context, query string) error {
	return ErrEngineNotImplemented
}

func (nqe *RemoteConnectionQueryEngine) Info() *models.Info {
	return &models.Info{
		Name:    NAME,
		Version: VERSION_NUMBER,
	}
}
