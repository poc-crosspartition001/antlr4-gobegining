package parser

// TraversalEdgeScope indicates the scope of edge traversal.
type TraversalEdgeScope string

const (
	// Native scope instructs traverser to look only for graph connections within partition
	NATIVE TraversalEdgeScope = "edgescope.native"

	// Remote scope instructs traverser to look only for graph connections across partitions
	REMOTE TraversalEdgeScope = "edgescope.remote"

	// All scope instructs traverser to look only for graph connections both within and across partitions
	ALL TraversalEdgeScope = "edgescope.all"
)
