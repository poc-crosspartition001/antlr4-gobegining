package parser

import (
	"bitbucket.org/cloudcoreo/go-services-sdk/gremlin-grammar/gremlin"
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"strings"
)

/**
 * A helper class that inherits from Antlr's generated listener used to traverse the AST when parsing/
 * validating a Gremlin query. The purpose of this class is mainly to determine if the gremlin query involves
 * retrieving remote connections.
 */
type RemoteConnectionParser struct {
	*gremlin.BasegremlinListener

	//vy- =========================================
	//sourceQuery strings.Builder
	//remoteQuery strings.Builder
	//SQ          string
	//RQ          string
	//Steps       []string
	//isRemote    bool
	//isFilter    bool
	//inFilter    bool

	RemoteDirection string

	//vy- =========================================

	// Tracks the native query through tree traversal
	nativeQuery strings.Builder

	// Indicates remote traversal scope is Remote and not all
	isRemoteOnly bool

	// Indicates if there is an active edge traversal
	inEdgeTraversal bool

	// Parser response query
	query *Query
}

//=========================================
//  following are from...
// bitbucket.org/cloudcoreo/go-services-sdk@v0.0.0-20220211192518-b640ac2b65cb/gremlin-grammar/gremlin
//  gremlin_base_listener.go

// VisitTerminal is called when a terminal node is visited.
func (s *RemoteConnectionParser) VisitTerminal(node antlr.TerminalNode) {
	fmt.Println("===============AAAAA   VisitTerminal")
}

// VisitErrorNode is called when an error node is visited.
func (s *RemoteConnectionParser) VisitErrorNode(node antlr.ErrorNode) {
	fmt.Println("===============AAAAA   VisitErrorNode")
}

// EnterEveryRule is called when any rule is entered.
func (s *RemoteConnectionParser) EnterEveryRule(ctx antlr.ParserRuleContext) {
	fmt.Println("===============AAAAA   EnterEveryRule")
	println(ctx.GetStart().GetText())
	println(ctx.GetText())
	fmt.Println("===============BBBBB   EnterEveryRule")
}

// ExitEveryRule is called when any rule is exited.
func (s *RemoteConnectionParser) ExitEveryRule(ctx antlr.ParserRuleContext) {
	fmt.Println("===============AAAAA   ExitEveryRule")
}

// EnterQuery is called when production query is entered.
func (s *RemoteConnectionParser) EnterQuery(ctx *gremlin.QueryContext) {
	fmt.Println("===============AAAAA   EnterQuery")
	println(ctx.GetText())
	println(ctx.GetStart().GetText())
	fmt.Println("===============BBBBB   EnterQuery")
}

// ExitQuery is called when production query is exited.
func (s *RemoteConnectionParser) ExitQuery(ctx *gremlin.QueryContext) {
	fmt.Println("===============AAAAA   ExitQuery")
}

// EnterTraversalSource is called when production traversalSource is entered.
func (s *RemoteConnectionParser) EnterTraversalSource(ctx *gremlin.TraversalSourceContext) {
	fmt.Println("===============AAAAA   EnterTraversalSource")
}

// ExitTraversalSource is called when production traversalSource is exited.
func (s *RemoteConnectionParser) ExitTraversalSource(ctx *gremlin.TraversalSourceContext) {
	fmt.Println("===============AAAAA   ExitTraversalSource")
}

// EnterRootTraversal is called when production rootTraversal is entered.
func (s *RemoteConnectionParser) EnterRootTraversal(ctx *gremlin.RootTraversalContext) {
	fmt.Println("===============AAAAA   EnterRootTraversal")
	println(ctx.GetStart().GetText())
	println(ctx.GetText())
	fmt.Println("===============BBBBB   EnterRootTraversal")

}

// ExitRootTraversal is called when production rootTraversal is exited.
func (s *RemoteConnectionParser) ExitRootTraversal(ctx *gremlin.RootTraversalContext) {
	fmt.Println("===============AAAAA   ExitRootTraversal")
}

// EnterTraversalSourceSelfMethod is called when production traversalSourceSelfMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod(ctx *gremlin.TraversalSourceSelfMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod")
}

// ExitTraversalSourceSelfMethod is called when production traversalSourceSelfMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod(ctx *gremlin.TraversalSourceSelfMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod")
}

// EnterTraversalSourceSelfMethod_withBulk is called when production traversalSourceSelfMethod_withBulk is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod_withBulk(ctx *gremlin.TraversalSourceSelfMethod_withBulkContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod_withBulk")
}

// ExitTraversalSourceSelfMethod_withBulk is called when production traversalSourceSelfMethod_withBulk is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod_withBulk(ctx *gremlin.TraversalSourceSelfMethod_withBulkContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod_withBulk")
}

// EnterTraversalSourceSelfMethod_withPath is called when production traversalSourceSelfMethod_withPath is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod_withPath(ctx *gremlin.TraversalSourceSelfMethod_withPathContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod_withPath")
}

// ExitTraversalSourceSelfMethod_withPath is called when production traversalSourceSelfMethod_withPath is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod_withPath(ctx *gremlin.TraversalSourceSelfMethod_withPathContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod_withPath")
}

// EnterTraversalSourceSelfMethod_withSack is called when production traversalSourceSelfMethod_withSack is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod_withSack(ctx *gremlin.TraversalSourceSelfMethod_withSackContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod_withSack")
}

// ExitTraversalSourceSelfMethod_withSack is called when production traversalSourceSelfMethod_withSack is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod_withSack(ctx *gremlin.TraversalSourceSelfMethod_withSackContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod_withSack")
}

// EnterTraversalSourceSelfMethod_withSideEffect is called when production traversalSourceSelfMethod_withSideEffect is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod_withSideEffect(ctx *gremlin.TraversalSourceSelfMethod_withSideEffectContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod_withSideEffect")
}

// ExitTraversalSourceSelfMethod_withSideEffect is called when production traversalSourceSelfMethod_withSideEffect is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod_withSideEffect(ctx *gremlin.TraversalSourceSelfMethod_withSideEffectContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod_withSideEffect")
}

// EnterTraversalSourceSelfMethod_with is called when production traversalSourceSelfMethod_with is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSelfMethod_with(ctx *gremlin.TraversalSourceSelfMethod_withContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSelfMethod_with")
}

// ExitTraversalSourceSelfMethod_with is called when production traversalSourceSelfMethod_with is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSelfMethod_with(ctx *gremlin.TraversalSourceSelfMethod_withContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSelfMethod_with")
}

// EnterTraversalSourceSpawnMethod is called when production traversalSourceSpawnMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSpawnMethod(ctx *gremlin.TraversalSourceSpawnMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSpawnMethod")
}

// ExitTraversalSourceSpawnMethod is called when production traversalSourceSpawnMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSpawnMethod(ctx *gremlin.TraversalSourceSpawnMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSpawnMethod")
}

// EnterTraversalSourceSpawnMethod_E is called when production traversalSourceSpawnMethod_E is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSpawnMethod_E(ctx *gremlin.TraversalSourceSpawnMethod_EContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSpawnMethod_E")
}

// ExitTraversalSourceSpawnMethod_E is called when production traversalSourceSpawnMethod_E is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSpawnMethod_E(ctx *gremlin.TraversalSourceSpawnMethod_EContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSpawnMethod_E")
}

// EnterTraversalSourceSpawnMethod_V is called when production traversalSourceSpawnMethod_V is entered.
func (s *RemoteConnectionParser) EnterTraversalSourceSpawnMethod_V(ctx *gremlin.TraversalSourceSpawnMethod_VContext) {
	fmt.Println("===============AAAAA   EnterTraversalSourceSpawnMethod_V")
}

// ExitTraversalSourceSpawnMethod_V is called when production traversalSourceSpawnMethod_V is exited.
func (s *RemoteConnectionParser) ExitTraversalSourceSpawnMethod_V(ctx *gremlin.TraversalSourceSpawnMethod_VContext) {
	fmt.Println("===============AAAAA   ExitTraversalSourceSpawnMethod_V")
}

// EnterChainedTraversal is called when production chainedTraversal is entered.
func (s *RemoteConnectionParser) EnterChainedTraversal(ctx *gremlin.ChainedTraversalContext) {
	fmt.Println("===============AAAAA   EnterChainedTraversal")
}

// ExitChainedTraversal is called when production chainedTraversal is exited.
func (s *RemoteConnectionParser) ExitChainedTraversal(ctx *gremlin.ChainedTraversalContext) {
	fmt.Println("===============AAAAA   ExitChainedTraversal")
}

// EnterChainedParentOfGraphTraversal is called when production chainedParentOfGraphTraversal is entered.
func (s *RemoteConnectionParser) EnterChainedParentOfGraphTraversal(ctx *gremlin.ChainedParentOfGraphTraversalContext) {
	fmt.Println("===============AAAAA   EnterChainedParentOfGraphTraversal")
}

// ExitChainedParentOfGraphTraversal is called when production chainedParentOfGraphTraversal is exited.
func (s *RemoteConnectionParser) ExitChainedParentOfGraphTraversal(ctx *gremlin.ChainedParentOfGraphTraversalContext) {
	fmt.Println("===============AAAAA   ExitChainedParentOfGraphTraversal")
}

// EnterNestedTraversal is called when production nestedTraversal is entered.
func (s *RemoteConnectionParser) EnterNestedTraversal(ctx *gremlin.NestedTraversalContext) {
	fmt.Println("===============AAAAA   EnterNestedTraversal")
}

// ExitNestedTraversal is called when production nestedTraversal is exited.
func (s *RemoteConnectionParser) ExitNestedTraversal(ctx *gremlin.NestedTraversalContext) {
	fmt.Println("===============AAAAA   ExitNestedTraversal")
}

// EnterTerminatedTraversal is called when production terminatedTraversal is entered.
func (s *RemoteConnectionParser) EnterTerminatedTraversal(ctx *gremlin.TerminatedTraversalContext) {
	fmt.Println("===============AAAAA   EnterTerminatedTraversal")
}

// ExitTerminatedTraversal is called when production terminatedTraversal is exited.
func (s *RemoteConnectionParser) ExitTerminatedTraversal(ctx *gremlin.TerminatedTraversalContext) {
	fmt.Println("===============AAAAA   ExitTerminatedTraversal")
}

// EnterTraversalMethod is called when production traversalMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod(ctx *gremlin.TraversalMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod")
	println(ctx.GetStart().GetText())
	println(ctx.GetText())
	fmt.Println("===============BBBBB   EnterTraversalMethod")
}

// ExitTraversalMethod is called when production traversalMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod(ctx *gremlin.TraversalMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod")
}

// EnterTraversalMethod_aggregate_Scope_String is called when production traversalMethod_aggregate_Scope_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_aggregate_Scope_String(ctx *gremlin.TraversalMethod_aggregate_Scope_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_aggregate_Scope_String")
}

// ExitTraversalMethod_aggregate_Scope_String is called when production traversalMethod_aggregate_Scope_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_aggregate_Scope_String(ctx *gremlin.TraversalMethod_aggregate_Scope_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_aggregate_Scope_String")
}

// EnterTraversalMethod_aggregate_String is called when production traversalMethod_aggregate_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_aggregate_String(ctx *gremlin.TraversalMethod_aggregate_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_aggregate_String")
}

// ExitTraversalMethod_aggregate_String is called when production traversalMethod_aggregate_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_aggregate_String(ctx *gremlin.TraversalMethod_aggregate_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_aggregate_String")
}

// EnterTraversalMethod_and is called when production traversalMethod_and is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_and(ctx *gremlin.TraversalMethod_andContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_and")
}

// ExitTraversalMethod_and is called when production traversalMethod_and is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_and(ctx *gremlin.TraversalMethod_andContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_and")
}

// EnterTraversalMethod_as is called when production traversalMethod_as is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_as(ctx *gremlin.TraversalMethod_asContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_as")
}

// ExitTraversalMethod_as is called when production traversalMethod_as is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_as(ctx *gremlin.TraversalMethod_asContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_as")
}

// EnterTraversalMethod_barrier_Consumer is called when production traversalMethod_barrier_Consumer is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_barrier_Consumer(ctx *gremlin.TraversalMethod_barrier_ConsumerContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_barrier_Consumer")
}

// ExitTraversalMethod_barrier_Consumer is called when production traversalMethod_barrier_Consumer is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_barrier_Consumer(ctx *gremlin.TraversalMethod_barrier_ConsumerContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_barrier_Consumer")
}

// EnterTraversalMethod_barrier_Empty is called when production traversalMethod_barrier_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_barrier_Empty(ctx *gremlin.TraversalMethod_barrier_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_barrier_Empty")
}

// ExitTraversalMethod_barrier_Empty is called when production traversalMethod_barrier_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_barrier_Empty(ctx *gremlin.TraversalMethod_barrier_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_barrier_Empty")
}

// EnterTraversalMethod_barrier_int is called when production traversalMethod_barrier_int is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_barrier_int(ctx *gremlin.TraversalMethod_barrier_intContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_barrier_int")
}

// ExitTraversalMethod_barrier_int is called when production traversalMethod_barrier_int is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_barrier_int(ctx *gremlin.TraversalMethod_barrier_intContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_barrier_int")
}

// EnterTraversalMethod_both is called when production traversalMethod_both is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_both(ctx *gremlin.TraversalMethod_bothContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_both")
}

// ExitTraversalMethod_both is called when production traversalMethod_both is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_both(ctx *gremlin.TraversalMethod_bothContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_both")
}

// EnterTraversalMethod_bothE is called when production traversalMethod_bothE is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_bothE(ctx *gremlin.TraversalMethod_bothEContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_bothE")
}

// ExitTraversalMethod_bothE is called when production traversalMethod_bothE is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_bothE(ctx *gremlin.TraversalMethod_bothEContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_bothE")
}

// EnterTraversalMethod_bothV is called when production traversalMethod_bothV is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_bothV(ctx *gremlin.TraversalMethod_bothVContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_bothV")
}

// ExitTraversalMethod_bothV is called when production traversalMethod_bothV is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_bothV(ctx *gremlin.TraversalMethod_bothVContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_bothV")
}

// EnterTraversalMethod_branch is called when production traversalMethod_branch is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_branch(ctx *gremlin.TraversalMethod_branchContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_branch")
}

// ExitTraversalMethod_branch is called when production traversalMethod_branch is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_branch(ctx *gremlin.TraversalMethod_branchContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_branch")
}

// EnterTraversalMethod_by_Comparator is called when production traversalMethod_by_Comparator is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Comparator(ctx *gremlin.TraversalMethod_by_ComparatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Comparator")
}

// ExitTraversalMethod_by_Comparator is called when production traversalMethod_by_Comparator is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Comparator(ctx *gremlin.TraversalMethod_by_ComparatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Comparator")
}

// EnterTraversalMethod_by_Empty is called when production traversalMethod_by_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Empty(ctx *gremlin.TraversalMethod_by_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Empty")
}

// ExitTraversalMethod_by_Empty is called when production traversalMethod_by_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Empty(ctx *gremlin.TraversalMethod_by_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Empty")
}

// EnterTraversalMethod_by_Function is called when production traversalMethod_by_Function is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Function(ctx *gremlin.TraversalMethod_by_FunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Function")
}

// ExitTraversalMethod_by_Function is called when production traversalMethod_by_Function is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Function(ctx *gremlin.TraversalMethod_by_FunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Function")
}

// EnterTraversalMethod_by_Function_Comparator is called when production traversalMethod_by_Function_Comparator is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Function_Comparator(ctx *gremlin.TraversalMethod_by_Function_ComparatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Function_Comparator")
}

// ExitTraversalMethod_by_Function_Comparator is called when production traversalMethod_by_Function_Comparator is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Function_Comparator(ctx *gremlin.TraversalMethod_by_Function_ComparatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Function_Comparator")
}

// EnterTraversalMethod_by_Order is called when production traversalMethod_by_Order is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Order(ctx *gremlin.TraversalMethod_by_OrderContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Order")
}

// ExitTraversalMethod_by_Order is called when production traversalMethod_by_Order is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Order(ctx *gremlin.TraversalMethod_by_OrderContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Order")
}

// EnterTraversalMethod_by_String is called when production traversalMethod_by_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_String(ctx *gremlin.TraversalMethod_by_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_String")
}

// ExitTraversalMethod_by_String is called when production traversalMethod_by_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_String(ctx *gremlin.TraversalMethod_by_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_String")
}

// EnterTraversalMethod_by_String_Comparator is called when production traversalMethod_by_String_Comparator is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_String_Comparator(ctx *gremlin.TraversalMethod_by_String_ComparatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_String_Comparator")
}

// ExitTraversalMethod_by_String_Comparator is called when production traversalMethod_by_String_Comparator is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_String_Comparator(ctx *gremlin.TraversalMethod_by_String_ComparatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_String_Comparator")
}

// EnterTraversalMethod_by_T is called when production traversalMethod_by_T is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_T(ctx *gremlin.TraversalMethod_by_TContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_T")
}

// ExitTraversalMethod_by_T is called when production traversalMethod_by_T is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_T(ctx *gremlin.TraversalMethod_by_TContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_T")
}

// EnterTraversalMethod_by_Traversal is called when production traversalMethod_by_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Traversal(ctx *gremlin.TraversalMethod_by_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Traversal")
}

// ExitTraversalMethod_by_Traversal is called when production traversalMethod_by_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Traversal(ctx *gremlin.TraversalMethod_by_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Traversal")
}

// EnterTraversalMethod_by_Traversal_Comparator is called when production traversalMethod_by_Traversal_Comparator is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_by_Traversal_Comparator(ctx *gremlin.TraversalMethod_by_Traversal_ComparatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_by_Traversal_Comparator")
}

// ExitTraversalMethod_by_Traversal_Comparator is called when production traversalMethod_by_Traversal_Comparator is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_by_Traversal_Comparator(ctx *gremlin.TraversalMethod_by_Traversal_ComparatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_by_Traversal_Comparator")
}

// EnterTraversalMethod_choose_Function is called when production traversalMethod_choose_Function is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Function(ctx *gremlin.TraversalMethod_choose_FunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Function")
}

// ExitTraversalMethod_choose_Function is called when production traversalMethod_choose_Function is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Function(ctx *gremlin.TraversalMethod_choose_FunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Function")
}

// EnterTraversalMethod_choose_Predicate_Traversal is called when production traversalMethod_choose_Predicate_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Predicate_Traversal(ctx *gremlin.TraversalMethod_choose_Predicate_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Predicate_Traversal")
}

// ExitTraversalMethod_choose_Predicate_Traversal is called when production traversalMethod_choose_Predicate_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Predicate_Traversal(ctx *gremlin.TraversalMethod_choose_Predicate_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Predicate_Traversal")
}

// EnterTraversalMethod_choose_Predicate_Traversal_Traversal is called when production traversalMethod_choose_Predicate_Traversal_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Predicate_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Predicate_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Predicate_Traversal_Traversal")
}

// ExitTraversalMethod_choose_Predicate_Traversal_Traversal is called when production traversalMethod_choose_Predicate_Traversal_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Predicate_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Predicate_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Predicate_Traversal_Traversal")
}

// EnterTraversalMethod_choose_Traversal is called when production traversalMethod_choose_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Traversal(ctx *gremlin.TraversalMethod_choose_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Traversal")
}

// ExitTraversalMethod_choose_Traversal is called when production traversalMethod_choose_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Traversal(ctx *gremlin.TraversalMethod_choose_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Traversal")
}

// EnterTraversalMethod_choose_Traversal_Traversal is called when production traversalMethod_choose_Traversal_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Traversal_Traversal")
}

// ExitTraversalMethod_choose_Traversal_Traversal is called when production traversalMethod_choose_Traversal_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Traversal_Traversal")
}

// EnterTraversalMethod_choose_Traversal_Traversal_Traversal is called when production traversalMethod_choose_Traversal_Traversal_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_choose_Traversal_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Traversal_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_choose_Traversal_Traversal_Traversal")
}

// ExitTraversalMethod_choose_Traversal_Traversal_Traversal is called when production traversalMethod_choose_Traversal_Traversal_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_choose_Traversal_Traversal_Traversal(ctx *gremlin.TraversalMethod_choose_Traversal_Traversal_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_choose_Traversal_Traversal_Traversal")
}

// EnterTraversalMethod_coalesce is called when production traversalMethod_coalesce is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_coalesce(ctx *gremlin.TraversalMethod_coalesceContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_coalesce")
}

// ExitTraversalMethod_coalesce is called when production traversalMethod_coalesce is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_coalesce(ctx *gremlin.TraversalMethod_coalesceContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_coalesce")
}

// EnterTraversalMethod_coin is called when production traversalMethod_coin is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_coin(ctx *gremlin.TraversalMethod_coinContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_coin")
}

// ExitTraversalMethod_coin is called when production traversalMethod_coin is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_coin(ctx *gremlin.TraversalMethod_coinContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_coin")
}

// EnterTraversalMethod_constant is called when production traversalMethod_constant is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_constant(ctx *gremlin.TraversalMethod_constantContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_constant")
}

// ExitTraversalMethod_constant is called when production traversalMethod_constant is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_constant(ctx *gremlin.TraversalMethod_constantContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_constant")
}

// EnterTraversalMethod_count_Empty is called when production traversalMethod_count_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_count_Empty(ctx *gremlin.TraversalMethod_count_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_count_Empty")
}

// ExitTraversalMethod_count_Empty is called when production traversalMethod_count_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_count_Empty(ctx *gremlin.TraversalMethod_count_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_count_Empty")
}

// EnterTraversalMethod_count_Scope is called when production traversalMethod_count_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_count_Scope(ctx *gremlin.TraversalMethod_count_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_count_Scope")
}

// ExitTraversalMethod_count_Scope is called when production traversalMethod_count_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_count_Scope(ctx *gremlin.TraversalMethod_count_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_count_Scope")
}

// EnterTraversalMethod_cyclicPath is called when production traversalMethod_cyclicPath is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_cyclicPath(ctx *gremlin.TraversalMethod_cyclicPathContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_cyclicPath")
}

// ExitTraversalMethod_cyclicPath is called when production traversalMethod_cyclicPath is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_cyclicPath(ctx *gremlin.TraversalMethod_cyclicPathContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_cyclicPath")
}

// EnterTraversalMethod_dedup_Scope_String is called when production traversalMethod_dedup_Scope_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_dedup_Scope_String(ctx *gremlin.TraversalMethod_dedup_Scope_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_dedup_Scope_String")
}

// ExitTraversalMethod_dedup_Scope_String is called when production traversalMethod_dedup_Scope_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_dedup_Scope_String(ctx *gremlin.TraversalMethod_dedup_Scope_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_dedup_Scope_String")
}

// EnterTraversalMethod_dedup_String is called when production traversalMethod_dedup_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_dedup_String(ctx *gremlin.TraversalMethod_dedup_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_dedup_String")
}

// ExitTraversalMethod_dedup_String is called when production traversalMethod_dedup_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_dedup_String(ctx *gremlin.TraversalMethod_dedup_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_dedup_String")
}

// EnterTraversalMethod_elementMap is called when production traversalMethod_elementMap is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_elementMap(ctx *gremlin.TraversalMethod_elementMapContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_elementMap")
}

// ExitTraversalMethod_elementMap is called when production traversalMethod_elementMap is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_elementMap(ctx *gremlin.TraversalMethod_elementMapContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_elementMap")
}

// EnterTraversalMethod_emit_Empty is called when production traversalMethod_emit_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_emit_Empty(ctx *gremlin.TraversalMethod_emit_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_emit_Empty")
}

// ExitTraversalMethod_emit_Empty is called when production traversalMethod_emit_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_emit_Empty(ctx *gremlin.TraversalMethod_emit_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_emit_Empty")
}

// EnterTraversalMethod_emit_Predicate is called when production traversalMethod_emit_Predicate is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_emit_Predicate(ctx *gremlin.TraversalMethod_emit_PredicateContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_emit_Predicate")
}

// ExitTraversalMethod_emit_Predicate is called when production traversalMethod_emit_Predicate is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_emit_Predicate(ctx *gremlin.TraversalMethod_emit_PredicateContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_emit_Predicate")
}

// EnterTraversalMethod_emit_Traversal is called when production traversalMethod_emit_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_emit_Traversal(ctx *gremlin.TraversalMethod_emit_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_emit_Traversal")
}

// ExitTraversalMethod_emit_Traversal is called when production traversalMethod_emit_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_emit_Traversal(ctx *gremlin.TraversalMethod_emit_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_emit_Traversal")
}

// EnterTraversalMethod_filter_Predicate is called when production traversalMethod_filter_Predicate is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_filter_Predicate(ctx *gremlin.TraversalMethod_filter_PredicateContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_filter_Predicate")
}

// ExitTraversalMethod_filter_Predicate is called when production traversalMethod_filter_Predicate is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_filter_Predicate(ctx *gremlin.TraversalMethod_filter_PredicateContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_filter_Predicate")
}

// EnterTraversalMethod_filter_Traversal is called when production traversalMethod_filter_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_filter_Traversal(ctx *gremlin.TraversalMethod_filter_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_filter_Traversal")
}

// ExitTraversalMethod_filter_Traversal is called when production traversalMethod_filter_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_filter_Traversal(ctx *gremlin.TraversalMethod_filter_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_filter_Traversal")
}

// EnterTraversalMethod_flatMap is called when production traversalMethod_flatMap is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_flatMap(ctx *gremlin.TraversalMethod_flatMapContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_flatMap")
}

// ExitTraversalMethod_flatMap is called when production traversalMethod_flatMap is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_flatMap(ctx *gremlin.TraversalMethod_flatMapContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_flatMap")
}

// EnterTraversalMethod_fold_Empty is called when production traversalMethod_fold_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_fold_Empty(ctx *gremlin.TraversalMethod_fold_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_fold_Empty")
}

// ExitTraversalMethod_fold_Empty is called when production traversalMethod_fold_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_fold_Empty(ctx *gremlin.TraversalMethod_fold_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_fold_Empty")
}

// EnterTraversalMethod_fold_Object_BiFunction is called when production traversalMethod_fold_Object_BiFunction is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_fold_Object_BiFunction(ctx *gremlin.TraversalMethod_fold_Object_BiFunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_fold_Object_BiFunction")
}

// ExitTraversalMethod_fold_Object_BiFunction is called when production traversalMethod_fold_Object_BiFunction is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_fold_Object_BiFunction(ctx *gremlin.TraversalMethod_fold_Object_BiFunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_fold_Object_BiFunction")
}

// EnterTraversalMethod_from_String is called when production traversalMethod_from_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_from_String(ctx *gremlin.TraversalMethod_from_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_from_String")
}

// ExitTraversalMethod_from_String is called when production traversalMethod_from_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_from_String(ctx *gremlin.TraversalMethod_from_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_from_String")
}

// EnterTraversalMethod_from_Traversal is called when production traversalMethod_from_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_from_Traversal(ctx *gremlin.TraversalMethod_from_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_from_Traversal")
}

// ExitTraversalMethod_from_Traversal is called when production traversalMethod_from_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_from_Traversal(ctx *gremlin.TraversalMethod_from_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_from_Traversal")
}

// EnterTraversalMethod_group_Empty is called when production traversalMethod_group_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_group_Empty(ctx *gremlin.TraversalMethod_group_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_group_Empty")
}

// ExitTraversalMethod_group_Empty is called when production traversalMethod_group_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_group_Empty(ctx *gremlin.TraversalMethod_group_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_group_Empty")
}

// EnterTraversalMethod_group_String is called when production traversalMethod_group_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_group_String(ctx *gremlin.TraversalMethod_group_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_group_String")
}

// ExitTraversalMethod_group_String is called when production traversalMethod_group_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_group_String(ctx *gremlin.TraversalMethod_group_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_group_String")
}

// EnterTraversalMethod_groupCount_Empty is called when production traversalMethod_groupCount_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_groupCount_Empty(ctx *gremlin.TraversalMethod_groupCount_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_groupCount_Empty")
}

// ExitTraversalMethod_groupCount_Empty is called when production traversalMethod_groupCount_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_groupCount_Empty(ctx *gremlin.TraversalMethod_groupCount_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_groupCount_Empty")
}

// EnterTraversalMethod_groupCount_String is called when production traversalMethod_groupCount_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_groupCount_String(ctx *gremlin.TraversalMethod_groupCount_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_groupCount_String")
}

// ExitTraversalMethod_groupCount_String is called when production traversalMethod_groupCount_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_groupCount_String(ctx *gremlin.TraversalMethod_groupCount_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_groupCount_String")
}

// EnterTraversalMethod_has_String is called when production traversalMethod_has_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String(ctx *gremlin.TraversalMethod_has_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String")
}

// ExitTraversalMethod_has_String is called when production traversalMethod_has_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String(ctx *gremlin.TraversalMethod_has_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String")
}

// EnterTraversalMethod_has_String_Object is called when production traversalMethod_has_String_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String_Object(ctx *gremlin.TraversalMethod_has_String_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String_Object")
}

// ExitTraversalMethod_has_String_Object is called when production traversalMethod_has_String_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String_Object(ctx *gremlin.TraversalMethod_has_String_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String_Object")
}

// EnterTraversalMethod_has_String_P is called when production traversalMethod_has_String_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String_P(ctx *gremlin.TraversalMethod_has_String_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String_P")
}

// ExitTraversalMethod_has_String_P is called when production traversalMethod_has_String_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String_P(ctx *gremlin.TraversalMethod_has_String_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String_P")
}

// EnterTraversalMethod_has_String_String_Object is called when production traversalMethod_has_String_String_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String_String_Object(ctx *gremlin.TraversalMethod_has_String_String_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String_String_Object")
}

// ExitTraversalMethod_has_String_String_Object is called when production traversalMethod_has_String_String_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String_String_Object(ctx *gremlin.TraversalMethod_has_String_String_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String_String_Object")
}

// EnterTraversalMethod_has_String_String_P is called when production traversalMethod_has_String_String_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String_String_P(ctx *gremlin.TraversalMethod_has_String_String_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String_String_P")
}

// ExitTraversalMethod_has_String_String_P is called when production traversalMethod_has_String_String_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String_String_P(ctx *gremlin.TraversalMethod_has_String_String_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String_String_P")
}

// EnterTraversalMethod_has_String_Traversal is called when production traversalMethod_has_String_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_String_Traversal(ctx *gremlin.TraversalMethod_has_String_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_String_Traversal")
}

// ExitTraversalMethod_has_String_Traversal is called when production traversalMethod_has_String_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_String_Traversal(ctx *gremlin.TraversalMethod_has_String_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_String_Traversal")
}

// EnterTraversalMethod_has_T_Object is called when production traversalMethod_has_T_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_T_Object(ctx *gremlin.TraversalMethod_has_T_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_T_Object")
}

// ExitTraversalMethod_has_T_Object is called when production traversalMethod_has_T_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_T_Object(ctx *gremlin.TraversalMethod_has_T_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_T_Object")
}

// EnterTraversalMethod_has_T_P is called when production traversalMethod_has_T_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_T_P(ctx *gremlin.TraversalMethod_has_T_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_T_P")
}

// ExitTraversalMethod_has_T_P is called when production traversalMethod_has_T_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_T_P(ctx *gremlin.TraversalMethod_has_T_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_T_P")
}

// EnterTraversalMethod_has_T_Traversal is called when production traversalMethod_has_T_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_has_T_Traversal(ctx *gremlin.TraversalMethod_has_T_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_has_T_Traversal")
}

// ExitTraversalMethod_has_T_Traversal is called when production traversalMethod_has_T_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_has_T_Traversal(ctx *gremlin.TraversalMethod_has_T_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_has_T_Traversal")
}

// EnterTraversalMethod_hasId_Object_Object is called when production traversalMethod_hasId_Object_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasId_Object_Object(ctx *gremlin.TraversalMethod_hasId_Object_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasId_Object_Object")
}

// ExitTraversalMethod_hasId_Object_Object is called when production traversalMethod_hasId_Object_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasId_Object_Object(ctx *gremlin.TraversalMethod_hasId_Object_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasId_Object_Object")
}

// EnterTraversalMethod_hasId_P is called when production traversalMethod_hasId_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasId_P(ctx *gremlin.TraversalMethod_hasId_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasId_P")
}

// ExitTraversalMethod_hasId_P is called when production traversalMethod_hasId_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasId_P(ctx *gremlin.TraversalMethod_hasId_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasId_P")
}

// EnterTraversalMethod_hasKey_P is called when production traversalMethod_hasKey_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasKey_P(ctx *gremlin.TraversalMethod_hasKey_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasKey_P")
}

// ExitTraversalMethod_hasKey_P is called when production traversalMethod_hasKey_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasKey_P(ctx *gremlin.TraversalMethod_hasKey_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasKey_P")
}

// EnterTraversalMethod_hasKey_String_String is called when production traversalMethod_hasKey_String_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasKey_String_String(ctx *gremlin.TraversalMethod_hasKey_String_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasKey_String_String")
}

// ExitTraversalMethod_hasKey_String_String is called when production traversalMethod_hasKey_String_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasKey_String_String(ctx *gremlin.TraversalMethod_hasKey_String_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasKey_String_String")
}

// EnterTraversalMethod_hasLabel_P is called when production traversalMethod_hasLabel_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasLabel_P(ctx *gremlin.TraversalMethod_hasLabel_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasLabel_P")
}

// ExitTraversalMethod_hasLabel_P is called when production traversalMethod_hasLabel_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasLabel_P(ctx *gremlin.TraversalMethod_hasLabel_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasLabel_P")
}

// EnterTraversalMethod_hasLabel_String_String is called when production traversalMethod_hasLabel_String_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasLabel_String_String(ctx *gremlin.TraversalMethod_hasLabel_String_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasLabel_String_String")
}

// ExitTraversalMethod_hasLabel_String_String is called when production traversalMethod_hasLabel_String_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasLabel_String_String(ctx *gremlin.TraversalMethod_hasLabel_String_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasLabel_String_String")
}

// EnterTraversalMethod_hasNot is called when production traversalMethod_hasNot is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasNot(ctx *gremlin.TraversalMethod_hasNotContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasNot")
}

// ExitTraversalMethod_hasNot is called when production traversalMethod_hasNot is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasNot(ctx *gremlin.TraversalMethod_hasNotContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasNot")
}

// EnterTraversalMethod_hasValue_Object_Object is called when production traversalMethod_hasValue_Object_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasValue_Object_Object(ctx *gremlin.TraversalMethod_hasValue_Object_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasValue_Object_Object")
}

// ExitTraversalMethod_hasValue_Object_Object is called when production traversalMethod_hasValue_Object_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasValue_Object_Object(ctx *gremlin.TraversalMethod_hasValue_Object_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasValue_Object_Object")
}

// EnterTraversalMethod_hasValue_P is called when production traversalMethod_hasValue_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_hasValue_P(ctx *gremlin.TraversalMethod_hasValue_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_hasValue_P")
}

// ExitTraversalMethod_hasValue_P is called when production traversalMethod_hasValue_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_hasValue_P(ctx *gremlin.TraversalMethod_hasValue_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_hasValue_P")
}

// EnterTraversalMethod_id is called when production traversalMethod_id is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_id(ctx *gremlin.TraversalMethod_idContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_id")
}

// ExitTraversalMethod_id is called when production traversalMethod_id is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_id(ctx *gremlin.TraversalMethod_idContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_id")
}

// EnterTraversalMethod_identity is called when production traversalMethod_identity is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_identity(ctx *gremlin.TraversalMethod_identityContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_identity")
}

// ExitTraversalMethod_identity is called when production traversalMethod_identity is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_identity(ctx *gremlin.TraversalMethod_identityContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_identity")
}

// EnterTraversalMethod_in is called when production traversalMethod_in is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_in(ctx *gremlin.TraversalMethod_inContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_in")
}

// ExitTraversalMethod_in is called when production traversalMethod_in is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_in(ctx *gremlin.TraversalMethod_inContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_in")
}

// EnterTraversalMethod_inE is called when production traversalMethod_inE is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_inE(ctx *gremlin.TraversalMethod_inEContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_inE")
}

// ExitTraversalMethod_inE is called when production traversalMethod_inE is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_inE(ctx *gremlin.TraversalMethod_inEContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_inE")
}

// EnterTraversalMethod_inV is called when production traversalMethod_inV is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_inV(ctx *gremlin.TraversalMethod_inVContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_inV")
}

// ExitTraversalMethod_inV is called when production traversalMethod_inV is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_inV(ctx *gremlin.TraversalMethod_inVContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_inV")
}

// EnterTraversalMethod_index is called when production traversalMethod_index is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_index(ctx *gremlin.TraversalMethod_indexContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_index")
}

// ExitTraversalMethod_index is called when production traversalMethod_index is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_index(ctx *gremlin.TraversalMethod_indexContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_index")
}

// EnterTraversalMethod_inject is called when production traversalMethod_inject is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_inject(ctx *gremlin.TraversalMethod_injectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_inject")
}

// ExitTraversalMethod_inject is called when production traversalMethod_inject is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_inject(ctx *gremlin.TraversalMethod_injectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_inject")
}

// EnterTraversalMethod_is_Object is called when production traversalMethod_is_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_is_Object(ctx *gremlin.TraversalMethod_is_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_is_Object")
}

// ExitTraversalMethod_is_Object is called when production traversalMethod_is_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_is_Object(ctx *gremlin.TraversalMethod_is_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_is_Object")
}

// EnterTraversalMethod_is_P is called when production traversalMethod_is_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_is_P(ctx *gremlin.TraversalMethod_is_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_is_P")
}

// ExitTraversalMethod_is_P is called when production traversalMethod_is_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_is_P(ctx *gremlin.TraversalMethod_is_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_is_P")
}

// EnterTraversalMethod_key is called when production traversalMethod_key is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_key(ctx *gremlin.TraversalMethod_keyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_key")
}

// ExitTraversalMethod_key is called when production traversalMethod_key is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_key(ctx *gremlin.TraversalMethod_keyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_key")
}

// EnterTraversalMethod_label is called when production traversalMethod_label is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_label(ctx *gremlin.TraversalMethod_labelContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_label")
}

// ExitTraversalMethod_label is called when production traversalMethod_label is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_label(ctx *gremlin.TraversalMethod_labelContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_label")
}

// EnterTraversalMethod_limit_Scope_long is called when production traversalMethod_limit_Scope_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_limit_Scope_long(ctx *gremlin.TraversalMethod_limit_Scope_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_limit_Scope_long")
}

// ExitTraversalMethod_limit_Scope_long is called when production traversalMethod_limit_Scope_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_limit_Scope_long(ctx *gremlin.TraversalMethod_limit_Scope_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_limit_Scope_long")
}

// EnterTraversalMethod_limit_long is called when production traversalMethod_limit_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_limit_long(ctx *gremlin.TraversalMethod_limit_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_limit_long")
}

// ExitTraversalMethod_limit_long is called when production traversalMethod_limit_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_limit_long(ctx *gremlin.TraversalMethod_limit_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_limit_long")
}

// EnterTraversalMethod_local is called when production traversalMethod_local is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_local(ctx *gremlin.TraversalMethod_localContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_local")
}

// ExitTraversalMethod_local is called when production traversalMethod_local is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_local(ctx *gremlin.TraversalMethod_localContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_local")
}

// EnterTraversalMethod_loops_Empty is called when production traversalMethod_loops_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_loops_Empty(ctx *gremlin.TraversalMethod_loops_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_loops_Empty")
}

// ExitTraversalMethod_loops_Empty is called when production traversalMethod_loops_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_loops_Empty(ctx *gremlin.TraversalMethod_loops_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_loops_Empty")
}

// EnterTraversalMethod_loops_String is called when production traversalMethod_loops_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_loops_String(ctx *gremlin.TraversalMethod_loops_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_loops_String")
}

// ExitTraversalMethod_loops_String is called when production traversalMethod_loops_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_loops_String(ctx *gremlin.TraversalMethod_loops_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_loops_String")
}

// EnterTraversalMethod_map is called when production traversalMethod_map is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_map(ctx *gremlin.TraversalMethod_mapContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_map")
}

// ExitTraversalMethod_map is called when production traversalMethod_map is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_map(ctx *gremlin.TraversalMethod_mapContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_map")
}

// EnterTraversalMethod_match is called when production traversalMethod_match is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_match(ctx *gremlin.TraversalMethod_matchContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_match")
}

// ExitTraversalMethod_match is called when production traversalMethod_match is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_match(ctx *gremlin.TraversalMethod_matchContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_match")
}

// EnterTraversalMethod_math is called when production traversalMethod_math is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_math(ctx *gremlin.TraversalMethod_mathContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_math")
}

// ExitTraversalMethod_math is called when production traversalMethod_math is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_math(ctx *gremlin.TraversalMethod_mathContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_math")
}

// EnterTraversalMethod_max_Empty is called when production traversalMethod_max_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_max_Empty(ctx *gremlin.TraversalMethod_max_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_max_Empty")
}

// ExitTraversalMethod_max_Empty is called when production traversalMethod_max_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_max_Empty(ctx *gremlin.TraversalMethod_max_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_max_Empty")
}

// EnterTraversalMethod_max_Scope is called when production traversalMethod_max_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_max_Scope(ctx *gremlin.TraversalMethod_max_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_max_Scope")
}

// ExitTraversalMethod_max_Scope is called when production traversalMethod_max_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_max_Scope(ctx *gremlin.TraversalMethod_max_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_max_Scope")
}

// EnterTraversalMethod_mean_Empty is called when production traversalMethod_mean_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_mean_Empty(ctx *gremlin.TraversalMethod_mean_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_mean_Empty")
}

// ExitTraversalMethod_mean_Empty is called when production traversalMethod_mean_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_mean_Empty(ctx *gremlin.TraversalMethod_mean_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_mean_Empty")
}

// EnterTraversalMethod_mean_Scope is called when production traversalMethod_mean_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_mean_Scope(ctx *gremlin.TraversalMethod_mean_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_mean_Scope")
}

// ExitTraversalMethod_mean_Scope is called when production traversalMethod_mean_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_mean_Scope(ctx *gremlin.TraversalMethod_mean_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_mean_Scope")
}

// EnterTraversalMethod_min_Empty is called when production traversalMethod_min_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_min_Empty(ctx *gremlin.TraversalMethod_min_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_min_Empty")
}

// ExitTraversalMethod_min_Empty is called when production traversalMethod_min_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_min_Empty(ctx *gremlin.TraversalMethod_min_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_min_Empty")
}

// EnterTraversalMethod_min_Scope is called when production traversalMethod_min_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_min_Scope(ctx *gremlin.TraversalMethod_min_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_min_Scope")
}

// ExitTraversalMethod_min_Scope is called when production traversalMethod_min_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_min_Scope(ctx *gremlin.TraversalMethod_min_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_min_Scope")
}

// EnterTraversalMethod_not is called when production traversalMethod_not is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_not(ctx *gremlin.TraversalMethod_notContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_not")
}

// ExitTraversalMethod_not is called when production traversalMethod_not is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_not(ctx *gremlin.TraversalMethod_notContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_not")
}

// EnterTraversalMethod_option_Predicate_Traversal is called when production traversalMethod_option_Predicate_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_option_Predicate_Traversal(ctx *gremlin.TraversalMethod_option_Predicate_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_option_Predicate_Traversal")
}

// ExitTraversalMethod_option_Predicate_Traversal is called when production traversalMethod_option_Predicate_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_option_Predicate_Traversal(ctx *gremlin.TraversalMethod_option_Predicate_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_option_Predicate_Traversal")
}

// EnterTraversalMethod_option_Object_Traversal is called when production traversalMethod_option_Object_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_option_Object_Traversal(ctx *gremlin.TraversalMethod_option_Object_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_option_Object_Traversal")
}

// ExitTraversalMethod_option_Object_Traversal is called when production traversalMethod_option_Object_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_option_Object_Traversal(ctx *gremlin.TraversalMethod_option_Object_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_option_Object_Traversal")
}

// EnterTraversalMethod_option_Traversal is called when production traversalMethod_option_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_option_Traversal(ctx *gremlin.TraversalMethod_option_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_option_Traversal")
}

// ExitTraversalMethod_option_Traversal is called when production traversalMethod_option_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_option_Traversal(ctx *gremlin.TraversalMethod_option_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_option_Traversal")
}

// EnterTraversalMethod_optional is called when production traversalMethod_optional is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_optional(ctx *gremlin.TraversalMethod_optionalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_optional")
}

// ExitTraversalMethod_optional is called when production traversalMethod_optional is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_optional(ctx *gremlin.TraversalMethod_optionalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_optional")
}

// EnterTraversalMethod_or is called when production traversalMethod_or is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_or(ctx *gremlin.TraversalMethod_orContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_or")
}

// ExitTraversalMethod_or is called when production traversalMethod_or is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_or(ctx *gremlin.TraversalMethod_orContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_or")
}

// EnterTraversalMethod_order_Empty is called when production traversalMethod_order_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_order_Empty(ctx *gremlin.TraversalMethod_order_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_order_Empty")
}

// ExitTraversalMethod_order_Empty is called when production traversalMethod_order_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_order_Empty(ctx *gremlin.TraversalMethod_order_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_order_Empty")
}

// EnterTraversalMethod_order_Scope is called when production traversalMethod_order_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_order_Scope(ctx *gremlin.TraversalMethod_order_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_order_Scope")
}

// ExitTraversalMethod_order_Scope is called when production traversalMethod_order_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_order_Scope(ctx *gremlin.TraversalMethod_order_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_order_Scope")
}

// EnterTraversalMethod_otherV is called when production traversalMethod_otherV is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_otherV(ctx *gremlin.TraversalMethod_otherVContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_otherV")
}

// ExitTraversalMethod_otherV is called when production traversalMethod_otherV is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_otherV(ctx *gremlin.TraversalMethod_otherVContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_otherV")
}

// EnterTraversalMethod_out is called when production traversalMethod_out is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_out(ctx *gremlin.TraversalMethod_outContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_out")
}

// ExitTraversalMethod_out is called when production traversalMethod_out is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_out(ctx *gremlin.TraversalMethod_outContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_out")
}

// EnterTraversalMethod_outE is called when production traversalMethod_outE is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_outE(ctx *gremlin.TraversalMethod_outEContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_outE")
}

// ExitTraversalMethod_outE is called when production traversalMethod_outE is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_outE(ctx *gremlin.TraversalMethod_outEContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_outE")
}

// EnterTraversalMethod_outV is called when production traversalMethod_outV is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_outV(ctx *gremlin.TraversalMethod_outVContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_outV")
}

// ExitTraversalMethod_outV is called when production traversalMethod_outV is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_outV(ctx *gremlin.TraversalMethod_outVContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_outV")
}

// EnterTraversalMethod_project is called when production traversalMethod_project is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_project(ctx *gremlin.TraversalMethod_projectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_project")
}

// ExitTraversalMethod_project is called when production traversalMethod_project is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_project(ctx *gremlin.TraversalMethod_projectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_project")
}

// EnterTraversalMethod_properties is called when production traversalMethod_properties is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_properties(ctx *gremlin.TraversalMethod_propertiesContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_properties")
}

// ExitTraversalMethod_properties is called when production traversalMethod_properties is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_properties(ctx *gremlin.TraversalMethod_propertiesContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_properties")
}

// EnterTraversalMethod_propertyMap is called when production traversalMethod_propertyMap is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_propertyMap(ctx *gremlin.TraversalMethod_propertyMapContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_propertyMap")
}

// ExitTraversalMethod_propertyMap is called when production traversalMethod_propertyMap is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_propertyMap(ctx *gremlin.TraversalMethod_propertyMapContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_propertyMap")
}

// EnterTraversalMethod_range_Scope_long_long is called when production traversalMethod_range_Scope_long_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_range_Scope_long_long(ctx *gremlin.TraversalMethod_range_Scope_long_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_range_Scope_long_long")
}

// ExitTraversalMethod_range_Scope_long_long is called when production traversalMethod_range_Scope_long_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_range_Scope_long_long(ctx *gremlin.TraversalMethod_range_Scope_long_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_range_Scope_long_long")
}

// EnterTraversalMethod_range_long_long is called when production traversalMethod_range_long_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_range_long_long(ctx *gremlin.TraversalMethod_range_long_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_range_long_long")
}

// ExitTraversalMethod_range_long_long is called when production traversalMethod_range_long_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_range_long_long(ctx *gremlin.TraversalMethod_range_long_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_range_long_long")
}

// EnterTraversalMethod_read is called when production traversalMethod_read is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_read(ctx *gremlin.TraversalMethod_readContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_read")
}

// ExitTraversalMethod_read is called when production traversalMethod_read is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_read(ctx *gremlin.TraversalMethod_readContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_read")
}

// EnterTraversalMethod_repeat_String_Traversal is called when production traversalMethod_repeat_String_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_repeat_String_Traversal(ctx *gremlin.TraversalMethod_repeat_String_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_repeat_String_Traversal")
}

// ExitTraversalMethod_repeat_String_Traversal is called when production traversalMethod_repeat_String_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_repeat_String_Traversal(ctx *gremlin.TraversalMethod_repeat_String_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_repeat_String_Traversal")
}

// EnterTraversalMethod_repeat_Traversal is called when production traversalMethod_repeat_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_repeat_Traversal(ctx *gremlin.TraversalMethod_repeat_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_repeat_Traversal")
}

// ExitTraversalMethod_repeat_Traversal is called when production traversalMethod_repeat_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_repeat_Traversal(ctx *gremlin.TraversalMethod_repeat_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_repeat_Traversal")
}

// EnterTraversalMethod_sack_BiFunction is called when production traversalMethod_sack_BiFunction is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sack_BiFunction(ctx *gremlin.TraversalMethod_sack_BiFunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sack_BiFunction")
}

// ExitTraversalMethod_sack_BiFunction is called when production traversalMethod_sack_BiFunction is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sack_BiFunction(ctx *gremlin.TraversalMethod_sack_BiFunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sack_BiFunction")
}

// EnterTraversalMethod_sack_Empty is called when production traversalMethod_sack_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sack_Empty(ctx *gremlin.TraversalMethod_sack_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sack_Empty")
}

// ExitTraversalMethod_sack_Empty is called when production traversalMethod_sack_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sack_Empty(ctx *gremlin.TraversalMethod_sack_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sack_Empty")
}

// EnterTraversalMethod_sample_Scope_int is called when production traversalMethod_sample_Scope_int is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sample_Scope_int(ctx *gremlin.TraversalMethod_sample_Scope_intContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sample_Scope_int")
}

// ExitTraversalMethod_sample_Scope_int is called when production traversalMethod_sample_Scope_int is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sample_Scope_int(ctx *gremlin.TraversalMethod_sample_Scope_intContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sample_Scope_int")
}

// EnterTraversalMethod_sample_int is called when production traversalMethod_sample_int is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sample_int(ctx *gremlin.TraversalMethod_sample_intContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sample_int")
}

// ExitTraversalMethod_sample_int is called when production traversalMethod_sample_int is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sample_int(ctx *gremlin.TraversalMethod_sample_intContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sample_int")
}

// EnterTraversalMethod_select_Column is called when production traversalMethod_select_Column is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_Column(ctx *gremlin.TraversalMethod_select_ColumnContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_Column")
}

// ExitTraversalMethod_select_Column is called when production traversalMethod_select_Column is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_Column(ctx *gremlin.TraversalMethod_select_ColumnContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_Column")
}

// EnterTraversalMethod_select_Pop_String is called when production traversalMethod_select_Pop_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_Pop_String(ctx *gremlin.TraversalMethod_select_Pop_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_Pop_String")
}

// ExitTraversalMethod_select_Pop_String is called when production traversalMethod_select_Pop_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_Pop_String(ctx *gremlin.TraversalMethod_select_Pop_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_Pop_String")
}

// EnterTraversalMethod_select_Pop_String_String_String is called when production traversalMethod_select_Pop_String_String_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_Pop_String_String_String(ctx *gremlin.TraversalMethod_select_Pop_String_String_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_Pop_String_String_String")
}

// ExitTraversalMethod_select_Pop_String_String_String is called when production traversalMethod_select_Pop_String_String_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_Pop_String_String_String(ctx *gremlin.TraversalMethod_select_Pop_String_String_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_Pop_String_String_String")
}

// EnterTraversalMethod_select_Pop_Traversal is called when production traversalMethod_select_Pop_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_Pop_Traversal(ctx *gremlin.TraversalMethod_select_Pop_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_Pop_Traversal")
}

// ExitTraversalMethod_select_Pop_Traversal is called when production traversalMethod_select_Pop_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_Pop_Traversal(ctx *gremlin.TraversalMethod_select_Pop_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_Pop_Traversal")
}

// EnterTraversalMethod_select_String is called when production traversalMethod_select_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_String(ctx *gremlin.TraversalMethod_select_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_String")
}

// ExitTraversalMethod_select_String is called when production traversalMethod_select_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_String(ctx *gremlin.TraversalMethod_select_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_String")
}

// EnterTraversalMethod_select_String_String_String is called when production traversalMethod_select_String_String_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_String_String_String(ctx *gremlin.TraversalMethod_select_String_String_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_String_String_String")
}

// ExitTraversalMethod_select_String_String_String is called when production traversalMethod_select_String_String_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_String_String_String(ctx *gremlin.TraversalMethod_select_String_String_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_String_String_String")
}

// EnterTraversalMethod_select_Traversal is called when production traversalMethod_select_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_select_Traversal(ctx *gremlin.TraversalMethod_select_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_select_Traversal")
}

// ExitTraversalMethod_select_Traversal is called when production traversalMethod_select_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_select_Traversal(ctx *gremlin.TraversalMethod_select_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_select_Traversal")
}

// EnterTraversalMethod_sideEffect is called when production traversalMethod_sideEffect is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sideEffect(ctx *gremlin.TraversalMethod_sideEffectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sideEffect")
}

// ExitTraversalMethod_sideEffect is called when production traversalMethod_sideEffect is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sideEffect(ctx *gremlin.TraversalMethod_sideEffectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sideEffect")
}

// EnterTraversalMethod_simplePath is called when production traversalMethod_simplePath is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_simplePath(ctx *gremlin.TraversalMethod_simplePathContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_simplePath")
}

// ExitTraversalMethod_simplePath is called when production traversalMethod_simplePath is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_simplePath(ctx *gremlin.TraversalMethod_simplePathContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_simplePath")
}

// EnterTraversalMethod_skip_Scope_long is called when production traversalMethod_skip_Scope_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_skip_Scope_long(ctx *gremlin.TraversalMethod_skip_Scope_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_skip_Scope_long")
}

// ExitTraversalMethod_skip_Scope_long is called when production traversalMethod_skip_Scope_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_skip_Scope_long(ctx *gremlin.TraversalMethod_skip_Scope_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_skip_Scope_long")
}

// EnterTraversalMethod_skip_long is called when production traversalMethod_skip_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_skip_long(ctx *gremlin.TraversalMethod_skip_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_skip_long")
}

// ExitTraversalMethod_skip_long is called when production traversalMethod_skip_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_skip_long(ctx *gremlin.TraversalMethod_skip_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_skip_long")
}

// EnterTraversalMethod_store is called when production traversalMethod_store is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_store(ctx *gremlin.TraversalMethod_storeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_store")
}

// ExitTraversalMethod_store is called when production traversalMethod_store is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_store(ctx *gremlin.TraversalMethod_storeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_store")
}

// EnterTraversalMethod_subgraph is called when production traversalMethod_subgraph is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_subgraph(ctx *gremlin.TraversalMethod_subgraphContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_subgraph")
}

// ExitTraversalMethod_subgraph is called when production traversalMethod_subgraph is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_subgraph(ctx *gremlin.TraversalMethod_subgraphContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_subgraph")
}

// EnterTraversalMethod_sum_Empty is called when production traversalMethod_sum_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sum_Empty(ctx *gremlin.TraversalMethod_sum_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sum_Empty")
}

// ExitTraversalMethod_sum_Empty is called when production traversalMethod_sum_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sum_Empty(ctx *gremlin.TraversalMethod_sum_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sum_Empty")
}

// EnterTraversalMethod_sum_Scope is called when production traversalMethod_sum_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_sum_Scope(ctx *gremlin.TraversalMethod_sum_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_sum_Scope")
}

// ExitTraversalMethod_sum_Scope is called when production traversalMethod_sum_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_sum_Scope(ctx *gremlin.TraversalMethod_sum_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_sum_Scope")
}

// EnterTraversalMethod_tail_Empty is called when production traversalMethod_tail_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tail_Empty(ctx *gremlin.TraversalMethod_tail_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tail_Empty")
}

// ExitTraversalMethod_tail_Empty is called when production traversalMethod_tail_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tail_Empty(ctx *gremlin.TraversalMethod_tail_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tail_Empty")
}

// EnterTraversalMethod_tail_Scope is called when production traversalMethod_tail_Scope is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tail_Scope(ctx *gremlin.TraversalMethod_tail_ScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tail_Scope")
}

// ExitTraversalMethod_tail_Scope is called when production traversalMethod_tail_Scope is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tail_Scope(ctx *gremlin.TraversalMethod_tail_ScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tail_Scope")
}

// EnterTraversalMethod_tail_Scope_long is called when production traversalMethod_tail_Scope_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tail_Scope_long(ctx *gremlin.TraversalMethod_tail_Scope_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tail_Scope_long")
}

// ExitTraversalMethod_tail_Scope_long is called when production traversalMethod_tail_Scope_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tail_Scope_long(ctx *gremlin.TraversalMethod_tail_Scope_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tail_Scope_long")
}

// EnterTraversalMethod_tail_long is called when production traversalMethod_tail_long is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tail_long(ctx *gremlin.TraversalMethod_tail_longContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tail_long")
}

// ExitTraversalMethod_tail_long is called when production traversalMethod_tail_long is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tail_long(ctx *gremlin.TraversalMethod_tail_longContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tail_long")
}

// EnterTraversalMethod_timeLimit is called when production traversalMethod_timeLimit is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_timeLimit(ctx *gremlin.TraversalMethod_timeLimitContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_timeLimit")
}

// ExitTraversalMethod_timeLimit is called when production traversalMethod_timeLimit is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_timeLimit(ctx *gremlin.TraversalMethod_timeLimitContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_timeLimit")
}

// EnterTraversalMethod_times is called when production traversalMethod_times is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_times(ctx *gremlin.TraversalMethod_timesContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_times")
}

// ExitTraversalMethod_times is called when production traversalMethod_times is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_times(ctx *gremlin.TraversalMethod_timesContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_times")
}

// EnterTraversalMethod_to_Direction_String is called when production traversalMethod_to_Direction_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_to_Direction_String(ctx *gremlin.TraversalMethod_to_Direction_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_to_Direction_String")
}

// ExitTraversalMethod_to_Direction_String is called when production traversalMethod_to_Direction_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_to_Direction_String(ctx *gremlin.TraversalMethod_to_Direction_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_to_Direction_String")
}

// EnterTraversalMethod_to_String is called when production traversalMethod_to_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_to_String(ctx *gremlin.TraversalMethod_to_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_to_String")
}

// ExitTraversalMethod_to_String is called when production traversalMethod_to_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_to_String(ctx *gremlin.TraversalMethod_to_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_to_String")
}

// EnterTraversalMethod_to_Traversal is called when production traversalMethod_to_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_to_Traversal(ctx *gremlin.TraversalMethod_to_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_to_Traversal")
}

// ExitTraversalMethod_to_Traversal is called when production traversalMethod_to_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_to_Traversal(ctx *gremlin.TraversalMethod_to_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_to_Traversal")
}

// EnterTraversalMethod_toE is called when production traversalMethod_toE is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_toE(ctx *gremlin.TraversalMethod_toEContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_toE")
}

// ExitTraversalMethod_toE is called when production traversalMethod_toE is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_toE(ctx *gremlin.TraversalMethod_toEContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_toE")
}

// EnterTraversalMethod_toV is called when production traversalMethod_toV is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_toV(ctx *gremlin.TraversalMethod_toVContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_toV")
}

// ExitTraversalMethod_toV is called when production traversalMethod_toV is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_toV(ctx *gremlin.TraversalMethod_toVContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_toV")
}

// EnterTraversalMethod_tree_Empty is called when production traversalMethod_tree_Empty is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tree_Empty(ctx *gremlin.TraversalMethod_tree_EmptyContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tree_Empty")
}

// ExitTraversalMethod_tree_Empty is called when production traversalMethod_tree_Empty is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tree_Empty(ctx *gremlin.TraversalMethod_tree_EmptyContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tree_Empty")
}

// EnterTraversalMethod_tree_String is called when production traversalMethod_tree_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_tree_String(ctx *gremlin.TraversalMethod_tree_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_tree_String")
}

// ExitTraversalMethod_tree_String is called when production traversalMethod_tree_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_tree_String(ctx *gremlin.TraversalMethod_tree_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_tree_String")
}

// EnterTraversalMethod_unfold is called when production traversalMethod_unfold is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_unfold(ctx *gremlin.TraversalMethod_unfoldContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_unfold")
}

// ExitTraversalMethod_unfold is called when production traversalMethod_unfold is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_unfold(ctx *gremlin.TraversalMethod_unfoldContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_unfold")
}

// EnterTraversalMethod_union is called when production traversalMethod_union is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_union(ctx *gremlin.TraversalMethod_unionContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_union")
}

// ExitTraversalMethod_union is called when production traversalMethod_union is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_union(ctx *gremlin.TraversalMethod_unionContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_union")
}

// EnterTraversalMethod_until_Predicate is called when production traversalMethod_until_Predicate is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_until_Predicate(ctx *gremlin.TraversalMethod_until_PredicateContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_until_Predicate")
}

// ExitTraversalMethod_until_Predicate is called when production traversalMethod_until_Predicate is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_until_Predicate(ctx *gremlin.TraversalMethod_until_PredicateContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_until_Predicate")
}

// EnterTraversalMethod_until_Traversal is called when production traversalMethod_until_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_until_Traversal(ctx *gremlin.TraversalMethod_until_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_until_Traversal")
}

// ExitTraversalMethod_until_Traversal is called when production traversalMethod_until_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_until_Traversal(ctx *gremlin.TraversalMethod_until_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_until_Traversal")
}

// EnterTraversalMethod_value is called when production traversalMethod_value is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_value(ctx *gremlin.TraversalMethod_valueContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_value")
}

// ExitTraversalMethod_value is called when production traversalMethod_value is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_value(ctx *gremlin.TraversalMethod_valueContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_value")
}

// EnterTraversalMethod_valueMap_String is called when production traversalMethod_valueMap_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_valueMap_String(ctx *gremlin.TraversalMethod_valueMap_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_valueMap_String")
}

// ExitTraversalMethod_valueMap_String is called when production traversalMethod_valueMap_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_valueMap_String(ctx *gremlin.TraversalMethod_valueMap_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_valueMap_String")
}

// EnterTraversalMethod_valueMap_boolean_String is called when production traversalMethod_valueMap_boolean_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_valueMap_boolean_String(ctx *gremlin.TraversalMethod_valueMap_boolean_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_valueMap_boolean_String")
}

// ExitTraversalMethod_valueMap_boolean_String is called when production traversalMethod_valueMap_boolean_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_valueMap_boolean_String(ctx *gremlin.TraversalMethod_valueMap_boolean_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_valueMap_boolean_String")
}

// EnterTraversalMethod_values is called when production traversalMethod_values is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_values(ctx *gremlin.TraversalMethod_valuesContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_values")
}

// ExitTraversalMethod_values is called when production traversalMethod_values is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_values(ctx *gremlin.TraversalMethod_valuesContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_values")
}

// EnterTraversalMethod_where_P is called when production traversalMethod_where_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_where_P(ctx *gremlin.TraversalMethod_where_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_where_P")
}

// ExitTraversalMethod_where_P is called when production traversalMethod_where_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_where_P(ctx *gremlin.TraversalMethod_where_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_where_P")
}

// EnterTraversalMethod_where_String_P is called when production traversalMethod_where_String_P is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_where_String_P(ctx *gremlin.TraversalMethod_where_String_PContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_where_String_P")
}

// ExitTraversalMethod_where_String_P is called when production traversalMethod_where_String_P is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_where_String_P(ctx *gremlin.TraversalMethod_where_String_PContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_where_String_P")
}

// EnterTraversalMethod_where_Traversal is called when production traversalMethod_where_Traversal is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_where_Traversal(ctx *gremlin.TraversalMethod_where_TraversalContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_where_Traversal")
}

// ExitTraversalMethod_where_Traversal is called when production traversalMethod_where_Traversal is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_where_Traversal(ctx *gremlin.TraversalMethod_where_TraversalContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_where_Traversal")
}

// EnterTraversalMethod_with_String is called when production traversalMethod_with_String is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_with_String(ctx *gremlin.TraversalMethod_with_StringContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_with_String")
}

// ExitTraversalMethod_with_String is called when production traversalMethod_with_String is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_with_String(ctx *gremlin.TraversalMethod_with_StringContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_with_String")
}

// EnterTraversalMethod_with_String_Object is called when production traversalMethod_with_String_Object is entered.
func (s *RemoteConnectionParser) EnterTraversalMethod_with_String_Object(ctx *gremlin.TraversalMethod_with_String_ObjectContext) {
	fmt.Println("===============AAAAA   EnterTraversalMethod_with_String_Object")
}

// ExitTraversalMethod_with_String_Object is called when production traversalMethod_with_String_Object is exited.
func (s *RemoteConnectionParser) ExitTraversalMethod_with_String_Object(ctx *gremlin.TraversalMethod_with_String_ObjectContext) {
	fmt.Println("===============AAAAA   ExitTraversalMethod_with_String_Object")
}

// EnterTraversalScope is called when production traversalScope is entered.
func (s *RemoteConnectionParser) EnterTraversalScope(ctx *gremlin.TraversalScopeContext) {
	fmt.Println("===============AAAAA   EnterTraversalScope")
}

// ExitTraversalScope is called when production traversalScope is exited.
func (s *RemoteConnectionParser) ExitTraversalScope(ctx *gremlin.TraversalScopeContext) {
	fmt.Println("===============AAAAA   ExitTraversalScope")
}

// EnterTraversalToken is called when production traversalToken is entered.
func (s *RemoteConnectionParser) EnterTraversalToken(ctx *gremlin.TraversalTokenContext) {
	fmt.Println("===============AAAAA   EnterTraversalToken")
}

// ExitTraversalToken is called when production traversalToken is exited.
func (s *RemoteConnectionParser) ExitTraversalToken(ctx *gremlin.TraversalTokenContext) {
	fmt.Println("===============AAAAA   ExitTraversalToken")
}

// EnterTraversalOrder is called when production traversalOrder is entered.
func (s *RemoteConnectionParser) EnterTraversalOrder(ctx *gremlin.TraversalOrderContext) {
	fmt.Println("===============AAAAA   EnterTraversalOrder")
}

// ExitTraversalOrder is called when production traversalOrder is exited.
func (s *RemoteConnectionParser) ExitTraversalOrder(ctx *gremlin.TraversalOrderContext) {
	fmt.Println("===============AAAAA   ExitTraversalOrder")
}

// EnterTraversalDirection is called when production traversalDirection is entered.
func (s *RemoteConnectionParser) EnterTraversalDirection(ctx *gremlin.TraversalDirectionContext) {
	fmt.Println("===============AAAAA   EnterTraversalDirection")
}

// ExitTraversalDirection is called when production traversalDirection is exited.
func (s *RemoteConnectionParser) ExitTraversalDirection(ctx *gremlin.TraversalDirectionContext) {
	fmt.Println("===============AAAAA   ExitTraversalDirection")
}

// EnterTraversalCardinality is called when production traversalCardinality is entered.
func (s *RemoteConnectionParser) EnterTraversalCardinality(ctx *gremlin.TraversalCardinalityContext) {
	fmt.Println("===============AAAAA   EnterTraversalCardinality")
}

// ExitTraversalCardinality is called when production traversalCardinality is exited.
func (s *RemoteConnectionParser) ExitTraversalCardinality(ctx *gremlin.TraversalCardinalityContext) {
	fmt.Println("===============AAAAA   ExitTraversalCardinality")
}

// EnterTraversalColumn is called when production traversalColumn is entered.
func (s *RemoteConnectionParser) EnterTraversalColumn(ctx *gremlin.TraversalColumnContext) {
	fmt.Println("===============AAAAA   EnterTraversalColumn")
}

// ExitTraversalColumn is called when production traversalColumn is exited.
func (s *RemoteConnectionParser) ExitTraversalColumn(ctx *gremlin.TraversalColumnContext) {
	fmt.Println("===============AAAAA   ExitTraversalColumn")
}

// EnterTraversalPop is called when production traversalPop is entered.
func (s *RemoteConnectionParser) EnterTraversalPop(ctx *gremlin.TraversalPopContext) {
	fmt.Println("===============AAAAA   EnterTraversalPop")
}

// ExitTraversalPop is called when production traversalPop is exited.
func (s *RemoteConnectionParser) ExitTraversalPop(ctx *gremlin.TraversalPopContext) {
	fmt.Println("===============AAAAA   ExitTraversalPop")
}

// EnterTraversalOperator is called when production traversalOperator is entered.
func (s *RemoteConnectionParser) EnterTraversalOperator(ctx *gremlin.TraversalOperatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalOperator")
}

// ExitTraversalOperator is called when production traversalOperator is exited.
func (s *RemoteConnectionParser) ExitTraversalOperator(ctx *gremlin.TraversalOperatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalOperator")
}

// EnterTraversalOptionParent is called when production traversalOptionParent is entered.
func (s *RemoteConnectionParser) EnterTraversalOptionParent(ctx *gremlin.TraversalOptionParentContext) {
	fmt.Println("===============AAAAA   EnterTraversalOptionParent")
}

// ExitTraversalOptionParent is called when production traversalOptionParent is exited.
func (s *RemoteConnectionParser) ExitTraversalOptionParent(ctx *gremlin.TraversalOptionParentContext) {
	fmt.Println("===============AAAAA   ExitTraversalOptionParent")
}

// EnterTraversalPredicate is called when production traversalPredicate is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate(ctx *gremlin.TraversalPredicateContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate")
}

// ExitTraversalPredicate is called when production traversalPredicate is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate(ctx *gremlin.TraversalPredicateContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate")
}

// EnterTraversalTerminalMethod is called when production traversalTerminalMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod(ctx *gremlin.TraversalTerminalMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod")
}

// ExitTraversalTerminalMethod is called when production traversalTerminalMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod(ctx *gremlin.TraversalTerminalMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod")
}

// EnterTraversalSackMethod is called when production traversalSackMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalSackMethod(ctx *gremlin.TraversalSackMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalSackMethod")
}

// ExitTraversalSackMethod is called when production traversalSackMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalSackMethod(ctx *gremlin.TraversalSackMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalSackMethod")
}

// EnterTraversalSelfMethod is called when production traversalSelfMethod is entered.
func (s *RemoteConnectionParser) EnterTraversalSelfMethod(ctx *gremlin.TraversalSelfMethodContext) {
	fmt.Println("===============AAAAA   EnterTraversalSelfMethod")
}

// ExitTraversalSelfMethod is called when production traversalSelfMethod is exited.
func (s *RemoteConnectionParser) ExitTraversalSelfMethod(ctx *gremlin.TraversalSelfMethodContext) {
	fmt.Println("===============AAAAA   ExitTraversalSelfMethod")
}

// EnterTraversalComparator is called when production traversalComparator is entered.
func (s *RemoteConnectionParser) EnterTraversalComparator(ctx *gremlin.TraversalComparatorContext) {
	fmt.Println("===============AAAAA   EnterTraversalComparator")
}

// ExitTraversalComparator is called when production traversalComparator is exited.
func (s *RemoteConnectionParser) ExitTraversalComparator(ctx *gremlin.TraversalComparatorContext) {
	fmt.Println("===============AAAAA   ExitTraversalComparator")
}

// EnterTraversalFunction is called when production traversalFunction is entered.
func (s *RemoteConnectionParser) EnterTraversalFunction(ctx *gremlin.TraversalFunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalFunction")
}

// ExitTraversalFunction is called when production traversalFunction is exited.
func (s *RemoteConnectionParser) ExitTraversalFunction(ctx *gremlin.TraversalFunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalFunction")
}

// EnterTraversalBiFunction is called when production traversalBiFunction is entered.
func (s *RemoteConnectionParser) EnterTraversalBiFunction(ctx *gremlin.TraversalBiFunctionContext) {
	fmt.Println("===============AAAAA   EnterTraversalBiFunction")
}

// ExitTraversalBiFunction is called when production traversalBiFunction is exited.
func (s *RemoteConnectionParser) ExitTraversalBiFunction(ctx *gremlin.TraversalBiFunctionContext) {
	fmt.Println("===============AAAAA   ExitTraversalBiFunction")
}

// EnterTraversalPredicate_eq is called when production traversalPredicate_eq is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_eq(ctx *gremlin.TraversalPredicate_eqContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_eq")
}

// ExitTraversalPredicate_eq is called when production traversalPredicate_eq is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_eq(ctx *gremlin.TraversalPredicate_eqContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_eq")
}

// EnterTraversalPredicate_neq is called when production traversalPredicate_neq is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_neq(ctx *gremlin.TraversalPredicate_neqContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_neq")
}

// ExitTraversalPredicate_neq is called when production traversalPredicate_neq is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_neq(ctx *gremlin.TraversalPredicate_neqContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_neq")
}

// EnterTraversalPredicate_lt is called when production traversalPredicate_lt is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_lt(ctx *gremlin.TraversalPredicate_ltContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_lt")
}

// ExitTraversalPredicate_lt is called when production traversalPredicate_lt is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_lt(ctx *gremlin.TraversalPredicate_ltContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_lt")
}

// EnterTraversalPredicate_lte is called when production traversalPredicate_lte is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_lte(ctx *gremlin.TraversalPredicate_lteContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_lte")
}

// ExitTraversalPredicate_lte is called when production traversalPredicate_lte is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_lte(ctx *gremlin.TraversalPredicate_lteContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_lte")
}

// EnterTraversalPredicate_gt is called when production traversalPredicate_gt is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_gt(ctx *gremlin.TraversalPredicate_gtContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_gt")
}

// ExitTraversalPredicate_gt is called when production traversalPredicate_gt is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_gt(ctx *gremlin.TraversalPredicate_gtContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_gt")
}

// EnterTraversalPredicate_gte is called when production traversalPredicate_gte is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_gte(ctx *gremlin.TraversalPredicate_gteContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_gte")
}

// ExitTraversalPredicate_gte is called when production traversalPredicate_gte is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_gte(ctx *gremlin.TraversalPredicate_gteContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_gte")
}

// EnterTraversalPredicate_inside is called when production traversalPredicate_inside is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_inside(ctx *gremlin.TraversalPredicate_insideContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_inside")
}

// ExitTraversalPredicate_inside is called when production traversalPredicate_inside is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_inside(ctx *gremlin.TraversalPredicate_insideContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_inside")
}

// EnterTraversalPredicate_outside is called when production traversalPredicate_outside is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_outside(ctx *gremlin.TraversalPredicate_outsideContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_outside")
}

// ExitTraversalPredicate_outside is called when production traversalPredicate_outside is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_outside(ctx *gremlin.TraversalPredicate_outsideContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_outside")
}

// EnterTraversalPredicate_between is called when production traversalPredicate_between is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_between(ctx *gremlin.TraversalPredicate_betweenContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_between")
}

// ExitTraversalPredicate_between is called when production traversalPredicate_between is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_between(ctx *gremlin.TraversalPredicate_betweenContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_between")
}

// EnterTraversalPredicate_within is called when production traversalPredicate_within is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_within(ctx *gremlin.TraversalPredicate_withinContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_within")
}

// ExitTraversalPredicate_within is called when production traversalPredicate_within is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_within(ctx *gremlin.TraversalPredicate_withinContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_within")
}

// EnterTraversalPredicate_without is called when production traversalPredicate_without is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_without(ctx *gremlin.TraversalPredicate_withoutContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_without")
}

// ExitTraversalPredicate_without is called when production traversalPredicate_without is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_without(ctx *gremlin.TraversalPredicate_withoutContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_without")
}

// EnterTraversalPredicate_not is called when production traversalPredicate_not is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_not(ctx *gremlin.TraversalPredicate_notContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_not")
}

// ExitTraversalPredicate_not is called when production traversalPredicate_not is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_not(ctx *gremlin.TraversalPredicate_notContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_not")
}

// EnterTraversalPredicate_containing is called when production traversalPredicate_containing is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_containing(ctx *gremlin.TraversalPredicate_containingContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_containing")
}

// ExitTraversalPredicate_containing is called when production traversalPredicate_containing is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_containing(ctx *gremlin.TraversalPredicate_containingContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_containing")
}

// EnterTraversalPredicate_notContaining is called when production traversalPredicate_notContaining is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_notContaining(ctx *gremlin.TraversalPredicate_notContainingContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_notContaining")
}

// ExitTraversalPredicate_notContaining is called when production traversalPredicate_notContaining is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_notContaining(ctx *gremlin.TraversalPredicate_notContainingContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_notContaining")
}

// EnterTraversalPredicate_startingWith is called when production traversalPredicate_startingWith is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_startingWith(ctx *gremlin.TraversalPredicate_startingWithContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_startingWith")
}

// ExitTraversalPredicate_startingWith is called when production traversalPredicate_startingWith is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_startingWith(ctx *gremlin.TraversalPredicate_startingWithContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_startingWith")
}

// EnterTraversalPredicate_notStartingWith is called when production traversalPredicate_notStartingWith is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_notStartingWith(ctx *gremlin.TraversalPredicate_notStartingWithContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_notStartingWith")
}

// ExitTraversalPredicate_notStartingWith is called when production traversalPredicate_notStartingWith is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_notStartingWith(ctx *gremlin.TraversalPredicate_notStartingWithContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_notStartingWith")
}

// EnterTraversalPredicate_endingWith is called when production traversalPredicate_endingWith is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_endingWith(ctx *gremlin.TraversalPredicate_endingWithContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_endingWith")
}

// ExitTraversalPredicate_endingWith is called when production traversalPredicate_endingWith is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_endingWith(ctx *gremlin.TraversalPredicate_endingWithContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_endingWith")
}

// EnterTraversalPredicate_notEndingWith is called when production traversalPredicate_notEndingWith is entered.
func (s *RemoteConnectionParser) EnterTraversalPredicate_notEndingWith(ctx *gremlin.TraversalPredicate_notEndingWithContext) {
	fmt.Println("===============AAAAA   EnterTraversalPredicate_notEndingWith")
}

// ExitTraversalPredicate_notEndingWith is called when production traversalPredicate_notEndingWith is exited.
func (s *RemoteConnectionParser) ExitTraversalPredicate_notEndingWith(ctx *gremlin.TraversalPredicate_notEndingWithContext) {
	fmt.Println("===============AAAAA   ExitTraversalPredicate_notEndingWith")
}

// EnterTraversalTerminalMethod_hasNext is called when production traversalTerminalMethod_hasNext is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_hasNext(ctx *gremlin.TraversalTerminalMethod_hasNextContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_hasNext")
}

// ExitTraversalTerminalMethod_hasNext is called when production traversalTerminalMethod_hasNext is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_hasNext(ctx *gremlin.TraversalTerminalMethod_hasNextContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_hasNext")
}

// EnterTraversalTerminalMethod_iterate is called when production traversalTerminalMethod_iterate is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_iterate(ctx *gremlin.TraversalTerminalMethod_iterateContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_iterate")
}

// ExitTraversalTerminalMethod_iterate is called when production traversalTerminalMethod_iterate is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_iterate(ctx *gremlin.TraversalTerminalMethod_iterateContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_iterate")
}

// EnterTraversalTerminalMethod_tryNext is called when production traversalTerminalMethod_tryNext is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_tryNext(ctx *gremlin.TraversalTerminalMethod_tryNextContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_tryNext")
}

// ExitTraversalTerminalMethod_tryNext is called when production traversalTerminalMethod_tryNext is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_tryNext(ctx *gremlin.TraversalTerminalMethod_tryNextContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_tryNext")
}

// EnterTraversalTerminalMethod_next is called when production traversalTerminalMethod_next is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_next(ctx *gremlin.TraversalTerminalMethod_nextContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_next")
}

// ExitTraversalTerminalMethod_next is called when production traversalTerminalMethod_next is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_next(ctx *gremlin.TraversalTerminalMethod_nextContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_next")
}

// EnterTraversalTerminalMethod_toList is called when production traversalTerminalMethod_toList is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_toList(ctx *gremlin.TraversalTerminalMethod_toListContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_toList")
}

// ExitTraversalTerminalMethod_toList is called when production traversalTerminalMethod_toList is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_toList(ctx *gremlin.TraversalTerminalMethod_toListContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_toList")
}

// EnterTraversalTerminalMethod_toSet is called when production traversalTerminalMethod_toSet is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_toSet(ctx *gremlin.TraversalTerminalMethod_toSetContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_toSet")
}

// ExitTraversalTerminalMethod_toSet is called when production traversalTerminalMethod_toSet is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_toSet(ctx *gremlin.TraversalTerminalMethod_toSetContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_toSet")
}

// EnterTraversalTerminalMethod_toBulkSet is called when production traversalTerminalMethod_toBulkSet is entered.
func (s *RemoteConnectionParser) EnterTraversalTerminalMethod_toBulkSet(ctx *gremlin.TraversalTerminalMethod_toBulkSetContext) {
	fmt.Println("===============AAAAA   EnterTraversalTerminalMethod_toBulkSet")
}

// ExitTraversalTerminalMethod_toBulkSet is called when production traversalTerminalMethod_toBulkSet is exited.
func (s *RemoteConnectionParser) ExitTraversalTerminalMethod_toBulkSet(ctx *gremlin.TraversalTerminalMethod_toBulkSetContext) {
	fmt.Println("===============AAAAA   ExitTraversalTerminalMethod_toBulkSet")
}

// EnterTraversalSelfMethod_none is called when production traversalSelfMethod_none is entered.
func (s *RemoteConnectionParser) EnterTraversalSelfMethod_none(ctx *gremlin.TraversalSelfMethod_noneContext) {
	fmt.Println("===============AAAAA   EnterTraversalSelfMethod_none")
}

// ExitTraversalSelfMethod_none is called when production traversalSelfMethod_none is exited.
func (s *RemoteConnectionParser) ExitTraversalSelfMethod_none(ctx *gremlin.TraversalSelfMethod_noneContext) {
	fmt.Println("===============AAAAA   ExitTraversalSelfMethod_none")
}

// EnterGremlinStringConstants is called when production gremlinStringConstants is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants(ctx *gremlin.GremlinStringConstantsContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants")
}

// ExitGremlinStringConstants is called when production gremlinStringConstants is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants(ctx *gremlin.GremlinStringConstantsContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants")
}

// EnterPageRankStringConstants is called when production pageRankStringConstants is entered.
func (s *RemoteConnectionParser) EnterPageRankStringConstants(ctx *gremlin.PageRankStringConstantsContext) {
	fmt.Println("===============AAAAA   EnterPageRankStringConstants")
}

// ExitPageRankStringConstants is called when production pageRankStringConstants is exited.
func (s *RemoteConnectionParser) ExitPageRankStringConstants(ctx *gremlin.PageRankStringConstantsContext) {
	fmt.Println("===============AAAAA   ExitPageRankStringConstants")
}

// EnterPeerPressureStringConstants is called when production peerPressureStringConstants is entered.
func (s *RemoteConnectionParser) EnterPeerPressureStringConstants(ctx *gremlin.PeerPressureStringConstantsContext) {
	fmt.Println("===============AAAAA   EnterPeerPressureStringConstants")
}

// ExitPeerPressureStringConstants is called when production peerPressureStringConstants is exited.
func (s *RemoteConnectionParser) ExitPeerPressureStringConstants(ctx *gremlin.PeerPressureStringConstantsContext) {
	fmt.Println("===============AAAAA   ExitPeerPressureStringConstants")
}

// EnterShortestPathStringConstants is called when production shortestPathStringConstants is entered.
func (s *RemoteConnectionParser) EnterShortestPathStringConstants(ctx *gremlin.ShortestPathStringConstantsContext) {
	fmt.Println("===============AAAAA   EnterShortestPathStringConstants")
}

// ExitShortestPathStringConstants is called when production shortestPathStringConstants is exited.
func (s *RemoteConnectionParser) ExitShortestPathStringConstants(ctx *gremlin.ShortestPathStringConstantsContext) {
	fmt.Println("===============AAAAA   ExitShortestPathStringConstants")
}

// EnterWithOptionsStringConstants is called when production withOptionsStringConstants is entered.
func (s *RemoteConnectionParser) EnterWithOptionsStringConstants(ctx *gremlin.WithOptionsStringConstantsContext) {
	fmt.Println("===============AAAAA   EnterWithOptionsStringConstants")
}

// ExitWithOptionsStringConstants is called when production withOptionsStringConstants is exited.
func (s *RemoteConnectionParser) ExitWithOptionsStringConstants(ctx *gremlin.WithOptionsStringConstantsContext) {
	fmt.Println("===============AAAAA   ExitWithOptionsStringConstants")
}

// EnterGremlinStringConstants_pageRankStringConstants_edges is called when production gremlinStringConstants_pageRankStringConstants_edges is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_pageRankStringConstants_edges(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_pageRankStringConstants_edges")
}

// ExitGremlinStringConstants_pageRankStringConstants_edges is called when production gremlinStringConstants_pageRankStringConstants_edges is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_pageRankStringConstants_edges(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_pageRankStringConstants_edges")
}

// EnterGremlinStringConstants_pageRankStringConstants_times is called when production gremlinStringConstants_pageRankStringConstants_times is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_pageRankStringConstants_times(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_timesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_pageRankStringConstants_times")
}

// ExitGremlinStringConstants_pageRankStringConstants_times is called when production gremlinStringConstants_pageRankStringConstants_times is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_pageRankStringConstants_times(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_timesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_pageRankStringConstants_times")
}

// EnterGremlinStringConstants_pageRankStringConstants_propertyName is called when production gremlinStringConstants_pageRankStringConstants_propertyName is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_pageRankStringConstants_propertyName(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_propertyNameContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_pageRankStringConstants_propertyName")
}

// ExitGremlinStringConstants_pageRankStringConstants_propertyName is called when production gremlinStringConstants_pageRankStringConstants_propertyName is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_pageRankStringConstants_propertyName(ctx *gremlin.GremlinStringConstants_pageRankStringConstants_propertyNameContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_pageRankStringConstants_propertyName")
}

// EnterGremlinStringConstants_peerPressureStringConstants_edges is called when production gremlinStringConstants_peerPressureStringConstants_edges is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_peerPressureStringConstants_edges(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_peerPressureStringConstants_edges")
}

// ExitGremlinStringConstants_peerPressureStringConstants_edges is called when production gremlinStringConstants_peerPressureStringConstants_edges is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_peerPressureStringConstants_edges(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_peerPressureStringConstants_edges")
}

// EnterGremlinStringConstants_peerPressureStringConstants_times is called when production gremlinStringConstants_peerPressureStringConstants_times is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_peerPressureStringConstants_times(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_timesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_peerPressureStringConstants_times")
}

// ExitGremlinStringConstants_peerPressureStringConstants_times is called when production gremlinStringConstants_peerPressureStringConstants_times is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_peerPressureStringConstants_times(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_timesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_peerPressureStringConstants_times")
}

// EnterGremlinStringConstants_peerPressureStringConstants_propertyName is called when production gremlinStringConstants_peerPressureStringConstants_propertyName is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_peerPressureStringConstants_propertyName(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_propertyNameContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_peerPressureStringConstants_propertyName")
}

// ExitGremlinStringConstants_peerPressureStringConstants_propertyName is called when production gremlinStringConstants_peerPressureStringConstants_propertyName is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_peerPressureStringConstants_propertyName(ctx *gremlin.GremlinStringConstants_peerPressureStringConstants_propertyNameContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_peerPressureStringConstants_propertyName")
}

// EnterGremlinStringConstants_shortestPathStringConstants_target is called when production gremlinStringConstants_shortestPathStringConstants_target is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_shortestPathStringConstants_target(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_targetContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_shortestPathStringConstants_target")
}

// ExitGremlinStringConstants_shortestPathStringConstants_target is called when production gremlinStringConstants_shortestPathStringConstants_target is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_shortestPathStringConstants_target(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_targetContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_shortestPathStringConstants_target")
}

// EnterGremlinStringConstants_shortestPathStringConstants_edges is called when production gremlinStringConstants_shortestPathStringConstants_edges is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_shortestPathStringConstants_edges(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_shortestPathStringConstants_edges")
}

// ExitGremlinStringConstants_shortestPathStringConstants_edges is called when production gremlinStringConstants_shortestPathStringConstants_edges is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_shortestPathStringConstants_edges(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_edgesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_shortestPathStringConstants_edges")
}

// EnterGremlinStringConstants_shortestPathStringConstants_distance is called when production gremlinStringConstants_shortestPathStringConstants_distance is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_shortestPathStringConstants_distance(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_distanceContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_shortestPathStringConstants_distance")
}

// ExitGremlinStringConstants_shortestPathStringConstants_distance is called when production gremlinStringConstants_shortestPathStringConstants_distance is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_shortestPathStringConstants_distance(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_distanceContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_shortestPathStringConstants_distance")
}

// EnterGremlinStringConstants_shortestPathStringConstants_maxDistance is called when production gremlinStringConstants_shortestPathStringConstants_maxDistance is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_shortestPathStringConstants_maxDistance(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_maxDistanceContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_shortestPathStringConstants_maxDistance")
}

// ExitGremlinStringConstants_shortestPathStringConstants_maxDistance is called when production gremlinStringConstants_shortestPathStringConstants_maxDistance is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_shortestPathStringConstants_maxDistance(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_maxDistanceContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_shortestPathStringConstants_maxDistance")
}

// EnterGremlinStringConstants_shortestPathStringConstants_includeEdges is called when production gremlinStringConstants_shortestPathStringConstants_includeEdges is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_shortestPathStringConstants_includeEdges(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_includeEdgesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_shortestPathStringConstants_includeEdges")
}

// ExitGremlinStringConstants_shortestPathStringConstants_includeEdges is called when production gremlinStringConstants_shortestPathStringConstants_includeEdges is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_shortestPathStringConstants_includeEdges(ctx *gremlin.GremlinStringConstants_shortestPathStringConstants_includeEdgesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_shortestPathStringConstants_includeEdges")
}

// EnterGremlinStringConstants_withOptionsStringConstants_tokens is called when production gremlinStringConstants_withOptionsStringConstants_tokens is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_tokens(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_tokensContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_tokens")
}

// ExitGremlinStringConstants_withOptionsStringConstants_tokens is called when production gremlinStringConstants_withOptionsStringConstants_tokens is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_tokens(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_tokensContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_tokens")
}

// EnterGremlinStringConstants_withOptionsStringConstants_none is called when production gremlinStringConstants_withOptionsStringConstants_none is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_none(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_noneContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_none")
}

// ExitGremlinStringConstants_withOptionsStringConstants_none is called when production gremlinStringConstants_withOptionsStringConstants_none is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_none(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_noneContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_none")
}

// EnterGremlinStringConstants_withOptionsStringConstants_ids is called when production gremlinStringConstants_withOptionsStringConstants_ids is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_ids(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_idsContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_ids")
}

// ExitGremlinStringConstants_withOptionsStringConstants_ids is called when production gremlinStringConstants_withOptionsStringConstants_ids is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_ids(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_idsContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_ids")
}

// EnterGremlinStringConstants_withOptionsStringConstants_labels is called when production gremlinStringConstants_withOptionsStringConstants_labels is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_labels(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_labelsContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_labels")
}

// ExitGremlinStringConstants_withOptionsStringConstants_labels is called when production gremlinStringConstants_withOptionsStringConstants_labels is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_labels(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_labelsContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_labels")
}

// EnterGremlinStringConstants_withOptionsStringConstants_keys is called when production gremlinStringConstants_withOptionsStringConstants_keys is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_keys(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_keysContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_keys")
}

// ExitGremlinStringConstants_withOptionsStringConstants_keys is called when production gremlinStringConstants_withOptionsStringConstants_keys is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_keys(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_keysContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_keys")
}

// EnterGremlinStringConstants_withOptionsStringConstants_values is called when production gremlinStringConstants_withOptionsStringConstants_values is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_values(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_valuesContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_values")
}

// ExitGremlinStringConstants_withOptionsStringConstants_values is called when production gremlinStringConstants_withOptionsStringConstants_values is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_values(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_valuesContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_values")
}

// EnterGremlinStringConstants_withOptionsStringConstants_all is called when production gremlinStringConstants_withOptionsStringConstants_all is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_all(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_allContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_all")
}

// ExitGremlinStringConstants_withOptionsStringConstants_all is called when production gremlinStringConstants_withOptionsStringConstants_all is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_all(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_allContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_all")
}

// EnterGremlinStringConstants_withOptionsStringConstants_indexer is called when production gremlinStringConstants_withOptionsStringConstants_indexer is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_indexer(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_indexerContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_indexer")
}

// ExitGremlinStringConstants_withOptionsStringConstants_indexer is called when production gremlinStringConstants_withOptionsStringConstants_indexer is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_indexer(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_indexerContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_indexer")
}

// EnterGremlinStringConstants_withOptionsStringConstants_list is called when production gremlinStringConstants_withOptionsStringConstants_list is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_list(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_listContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_list")
}

// ExitGremlinStringConstants_withOptionsStringConstants_list is called when production gremlinStringConstants_withOptionsStringConstants_list is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_list(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_listContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_list")
}

// EnterGremlinStringConstants_withOptionsStringConstants_map is called when production gremlinStringConstants_withOptionsStringConstants_map is entered.
func (s *RemoteConnectionParser) EnterGremlinStringConstants_withOptionsStringConstants_map(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_mapContext) {
	fmt.Println("===============AAAAA   EnterGremlinStringConstants_withOptionsStringConstants_map")
}

// ExitGremlinStringConstants_withOptionsStringConstants_map is called when production gremlinStringConstants_withOptionsStringConstants_map is exited.
func (s *RemoteConnectionParser) ExitGremlinStringConstants_withOptionsStringConstants_map(ctx *gremlin.GremlinStringConstants_withOptionsStringConstants_mapContext) {
	fmt.Println("===============AAAAA   ExitGremlinStringConstants_withOptionsStringConstants_map")
}

// EnterPageRankStringConstant is called when production pageRankStringConstant is entered.
func (s *RemoteConnectionParser) EnterPageRankStringConstant(ctx *gremlin.PageRankStringConstantContext) {
	fmt.Println("===============AAAAA   EnterPageRankStringConstant")
}

// ExitPageRankStringConstant is called when production pageRankStringConstant is exited.
func (s *RemoteConnectionParser) ExitPageRankStringConstant(ctx *gremlin.PageRankStringConstantContext) {
	fmt.Println("===============AAAAA   ExitPageRankStringConstant")
}

// EnterPeerPressureStringConstant is called when production peerPressureStringConstant is entered.
func (s *RemoteConnectionParser) EnterPeerPressureStringConstant(ctx *gremlin.PeerPressureStringConstantContext) {
	fmt.Println("===============AAAAA   EnterPeerPressureStringConstant")
}

// ExitPeerPressureStringConstant is called when production peerPressureStringConstant is exited.
func (s *RemoteConnectionParser) ExitPeerPressureStringConstant(ctx *gremlin.PeerPressureStringConstantContext) {
	fmt.Println("===============AAAAA   ExitPeerPressureStringConstant")
}

// EnterShortestPathStringConstant is called when production shortestPathStringConstant is entered.
func (s *RemoteConnectionParser) EnterShortestPathStringConstant(ctx *gremlin.ShortestPathStringConstantContext) {
	fmt.Println("===============AAAAA   EnterShortestPathStringConstant")
}

// ExitShortestPathStringConstant is called when production shortestPathStringConstant is exited.
func (s *RemoteConnectionParser) ExitShortestPathStringConstant(ctx *gremlin.ShortestPathStringConstantContext) {
	fmt.Println("===============AAAAA   ExitShortestPathStringConstant")
}

// EnterWithOptionsStringConstant is called when production withOptionsStringConstant is entered.
func (s *RemoteConnectionParser) EnterWithOptionsStringConstant(ctx *gremlin.WithOptionsStringConstantContext) {
	fmt.Println("===============AAAAA   EnterWithOptionsStringConstant")
}

// ExitWithOptionsStringConstant is called when production withOptionsStringConstant is exited.
func (s *RemoteConnectionParser) ExitWithOptionsStringConstant(ctx *gremlin.WithOptionsStringConstantContext) {
	fmt.Println("===============AAAAA   ExitWithOptionsStringConstant")
}

// EnterNestedTraversalList is called when production nestedTraversalList is entered.
func (s *RemoteConnectionParser) EnterNestedTraversalList(ctx *gremlin.NestedTraversalListContext) {
	fmt.Println("===============AAAAA   EnterNestedTraversalList")
}

// ExitNestedTraversalList is called when production nestedTraversalList is exited.
func (s *RemoteConnectionParser) ExitNestedTraversalList(ctx *gremlin.NestedTraversalListContext) {
	fmt.Println("===============AAAAA   ExitNestedTraversalList")
}

// EnterNestedTraversalExpr is called when production nestedTraversalExpr is entered.
func (s *RemoteConnectionParser) EnterNestedTraversalExpr(ctx *gremlin.NestedTraversalExprContext) {
	fmt.Println("===============AAAAA   EnterNestedTraversalExpr")
}

// ExitNestedTraversalExpr is called when production nestedTraversalExpr is exited.
func (s *RemoteConnectionParser) ExitNestedTraversalExpr(ctx *gremlin.NestedTraversalExprContext) {
	fmt.Println("===============AAAAA   ExitNestedTraversalExpr")
}

// EnterGenericLiteralList is called when production genericLiteralList is entered.
func (s *RemoteConnectionParser) EnterGenericLiteralList(ctx *gremlin.GenericLiteralListContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteralList")
}

// ExitGenericLiteralList is called when production genericLiteralList is exited.
func (s *RemoteConnectionParser) ExitGenericLiteralList(ctx *gremlin.GenericLiteralListContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteralList")
}

// EnterGenericLiteralExpr is called when production genericLiteralExpr is entered.
func (s *RemoteConnectionParser) EnterGenericLiteralExpr(ctx *gremlin.GenericLiteralExprContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteralExpr")
}

// ExitGenericLiteralExpr is called when production genericLiteralExpr is exited.
func (s *RemoteConnectionParser) ExitGenericLiteralExpr(ctx *gremlin.GenericLiteralExprContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteralExpr")
}

// EnterGenericLiteralRange is called when production genericLiteralRange is entered.
func (s *RemoteConnectionParser) EnterGenericLiteralRange(ctx *gremlin.GenericLiteralRangeContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteralRange")
}

// ExitGenericLiteralRange is called when production genericLiteralRange is exited.
func (s *RemoteConnectionParser) ExitGenericLiteralRange(ctx *gremlin.GenericLiteralRangeContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteralRange")
}

// EnterGenericLiteralCollection is called when production genericLiteralCollection is entered.
func (s *RemoteConnectionParser) EnterGenericLiteralCollection(ctx *gremlin.GenericLiteralCollectionContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteralCollection")
}

// ExitGenericLiteralCollection is called when production genericLiteralCollection is exited.
func (s *RemoteConnectionParser) ExitGenericLiteralCollection(ctx *gremlin.GenericLiteralCollectionContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteralCollection")
}

// EnterStringLiteralList is called when production stringLiteralList is entered.
func (s *RemoteConnectionParser) EnterStringLiteralList(ctx *gremlin.StringLiteralListContext) {
	fmt.Println("===============AAAAA   EnterStringLiteralList")
}

// ExitStringLiteralList is called when production stringLiteralList is exited.
func (s *RemoteConnectionParser) ExitStringLiteralList(ctx *gremlin.StringLiteralListContext) {
	fmt.Println("===============AAAAA   ExitStringLiteralList")
}

// EnterStringLiteralExpr is called when production stringLiteralExpr is entered.
func (s *RemoteConnectionParser) EnterStringLiteralExpr(ctx *gremlin.StringLiteralExprContext) {
	fmt.Println("===============AAAAA   EnterStringLiteralExpr")
}

// ExitStringLiteralExpr is called when production stringLiteralExpr is exited.
func (s *RemoteConnectionParser) ExitStringLiteralExpr(ctx *gremlin.StringLiteralExprContext) {
	fmt.Println("===============AAAAA   ExitStringLiteralExpr")
}

// EnterGenericLiteral is called when production genericLiteral is entered.
func (s *RemoteConnectionParser) EnterGenericLiteral(ctx *gremlin.GenericLiteralContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteral")
}

// ExitGenericLiteral is called when production genericLiteral is exited.
func (s *RemoteConnectionParser) ExitGenericLiteral(ctx *gremlin.GenericLiteralContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteral")
}

// EnterGenericLiteralMap is called when production genericLiteralMap is entered.
func (s *RemoteConnectionParser) EnterGenericLiteralMap(ctx *gremlin.GenericLiteralMapContext) {
	fmt.Println("===============AAAAA   EnterGenericLiteralMap")
}

// ExitGenericLiteralMap is called when production genericLiteralMap is exited.
func (s *RemoteConnectionParser) ExitGenericLiteralMap(ctx *gremlin.GenericLiteralMapContext) {
	fmt.Println("===============AAAAA   ExitGenericLiteralMap")
}

// EnterIntegerLiteral is called when production integerLiteral is entered.
func (s *RemoteConnectionParser) EnterIntegerLiteral(ctx *gremlin.IntegerLiteralContext) {
	fmt.Println("===============AAAAA   EnterIntegerLiteral")
}

// ExitIntegerLiteral is called when production integerLiteral is exited.
func (s *RemoteConnectionParser) ExitIntegerLiteral(ctx *gremlin.IntegerLiteralContext) {
	fmt.Println("===============AAAAA   ExitIntegerLiteral")
}

// EnterFloatLiteral is called when production floatLiteral is entered.
func (s *RemoteConnectionParser) EnterFloatLiteral(ctx *gremlin.FloatLiteralContext) {
	fmt.Println("===============AAAAA   EnterFloatLiteral")
}

// ExitFloatLiteral is called when production floatLiteral is exited.
func (s *RemoteConnectionParser) ExitFloatLiteral(ctx *gremlin.FloatLiteralContext) {
	fmt.Println("===============AAAAA   ExitFloatLiteral")
}

// EnterBooleanLiteral is called when production booleanLiteral is entered.
func (s *RemoteConnectionParser) EnterBooleanLiteral(ctx *gremlin.BooleanLiteralContext) {
	fmt.Println("===============AAAAA   EnterBooleanLiteral")
}

// ExitBooleanLiteral is called when production booleanLiteral is exited.
func (s *RemoteConnectionParser) ExitBooleanLiteral(ctx *gremlin.BooleanLiteralContext) {
	fmt.Println("===============AAAAA   ExitBooleanLiteral")
}

// EnterStringLiteral is called when production stringLiteral is entered.
func (s *RemoteConnectionParser) EnterStringLiteral(ctx *gremlin.StringLiteralContext) {
	fmt.Println("===============AAAAA   EnterStringLiteral")
}

// ExitStringLiteral is called when production stringLiteral is exited.
func (s *RemoteConnectionParser) ExitStringLiteral(ctx *gremlin.StringLiteralContext) {
	fmt.Println("===============AAAAA   ExitStringLiteral")
}

// EnterDateLiteral is called when production dateLiteral is entered.
func (s *RemoteConnectionParser) EnterDateLiteral(ctx *gremlin.DateLiteralContext) {
	fmt.Println("===============AAAAA   EnterDateLiteral")
}

// ExitDateLiteral is called when production dateLiteral is exited.
func (s *RemoteConnectionParser) ExitDateLiteral(ctx *gremlin.DateLiteralContext) {
	fmt.Println("===============AAAAA   ExitDateLiteral")
}

// EnterRelativeLiteral is called when production relativeLiteral is entered.
func (s *RemoteConnectionParser) EnterRelativeLiteral(ctx *gremlin.RelativeLiteralContext) {
	fmt.Println("===============AAAAA   EnterRelativeLiteral")
}

// ExitRelativeLiteral is called when production relativeLiteral is exited.
func (s *RemoteConnectionParser) ExitRelativeLiteral(ctx *gremlin.RelativeLiteralContext) {
	fmt.Println("===============AAAAA   ExitRelativeLiteral")
}

// EnterNullLiteral is called when production nullLiteral is entered.
func (s *RemoteConnectionParser) EnterNullLiteral(ctx *gremlin.NullLiteralContext) {
	fmt.Println("===============AAAAA   EnterNullLiteral")
}

// ExitNullLiteral is called when production nullLiteral is exited.
func (s *RemoteConnectionParser) ExitNullLiteral(ctx *gremlin.NullLiteralContext) {
	fmt.Println("===============AAAAA   ExitNullLiteral")
}
