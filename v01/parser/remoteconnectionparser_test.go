package parser

import (
	"bitbucket.org/cloudcoreo/go-services-sdk/gremlin-grammar/gremlin"
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"testing"
)

func TestBegining001(t *testing.T) {
	//  will just run each step of a query
	GremFiles := "../../GremFiles/"
	GremQueries := []string{
		"gremfile1100",
		"gremfile1110",
		"gremfile1120",
	}

	for _, k := range GremQueries {
		fmt.Println(k)
		txtPath := GremFiles + k + ".txt"
		fmt.Println(txtPath)

		is, _ := antlr.NewFileStream(txtPath)
		lexer := gremlin.NewgremlinLexer(is)
		stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
		p := gremlin.NewgremlinParser(stream)
		tree := p.Query()
		antlr.NewParseTreeWalker().Walk(&RemoteConnectionParser{}, tree)
	}

}

//======================================================
//======================================================
//======================================================
//======================================================
//func TestRemoteConnectionParserNativeQuery(t *testing.T) {
//
//	testCases := []struct {
//		gremlinQuery string
//		nativeQuery  *string
//	}{
//		{ // Case 1: Native query
//			gremlinQuery: "g.V().out('IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 2: Out with all
//			gremlinQuery: "g.V().out('edgescope.all','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 3: Out with remote
//			gremlinQuery: "g.V().out('edgescope.remote','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  nil,
//		},
//		{ // Case 4: Out with native
//			gremlinQuery: "g.V().out('edgescope.native','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 5: In with all
//			gremlinQuery: "g.V().out('IsAssociatedWith').in('edgescope.all').has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 6: In with remote
//			gremlinQuery: "g.V().out('IsAssociatedWith').in('edgescope.remote').has('s','y').count()",
//			nativeQuery:  nil,
//		},
//		{ // Case 7: In with native
//			gremlinQuery: "g.V().out('IsAssociatedWith').in(\"edgescope.native\").has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 8: OutE with all
//			gremlinQuery: "g.V().outE('edgescope.all','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().outE('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 9: OutE with remote
//			gremlinQuery: "g.V().outE('edgescope.remote','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  nil,
//		},
//		{ // Case 10: OutE with native
//			gremlinQuery: "g.V().outE('edgescope.native','IsAssociatedWith').in().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().outE('IsAssociatedWith').in().has('s','y').count()"),
//		},
//		{ // Case 11: InE with all
//			gremlinQuery: "g.V().out('IsAssociatedWith').inE('edgescope.all').has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').inE().has('s','y').count()"),
//		},
//		{ // Case 12: InE with remote
//			gremlinQuery: "g.V().out('IsAssociatedWith').inE('edgescope.remote').has('s','y').count()",
//			nativeQuery:  nil,
//		},
//		{ // Case 13: InE with native
//			gremlinQuery: "g.V().out('IsAssociatedWith').inE(\"edgescope.native\").has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.V().out('IsAssociatedWith').inE().has('s','y').count()"),
//		},
//		{ // Case 14: E with all
//			gremlinQuery: "g.E('edgescope.all',124).out('IsAssociatedWith').inE().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.E(124).out('IsAssociatedWith').inE().has('s','y').count()"),
//		},
//		{ // Case 15: E with remote
//			gremlinQuery: "g.E('edgescope.remote',124).out('IsAssociatedWith').inE().has('s','y').count()",
//			nativeQuery:  nil,
//		},
//		{ // Case 16: InE with native
//			gremlinQuery: "g.E('edgescope.native',124).out('IsAssociatedWith').inE().has('s','y').count()",
//			nativeQuery:  utils.ToStringPtr("g.E(124).out('IsAssociatedWith').inE().has('s','y').count()"),
//		},
//	}
//
//	if 1 == 1 {
//		fmt.Println("========  1st tests")
//
//		for i, tc := range testCases {
//			parser := &RemoteConnectionParser{}
//			inputStream := antlr.NewInputStream(tc.gremlinQuery)
//			lexer := gremlin.NewgremlinLexer(inputStream)
//			stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
//
//			gParser := gremlin.NewgremlinParser(stream)
//			gParser.BuildParseTrees = true
//
//			tree := gParser.Query()
//
//			antlr.NewParseTreeWalker().Walk(parser, tree)
//
//			if parser.query == nil {
//				t.Error("expected parser to have query initialized")
//				return
//			}
//
//			if !reflect.DeepEqual(parser.query.NativeQuery, tc.nativeQuery) {
//				t.Error("native query is not as expected", i+1, parser.nativeQuery.String())
//				return
//			}
//		}
//		//=  remove temporarily for dev
//	}
//}
//
//func TestRemoteConnectionParserRemoteQuery(t *testing.T) {
//	testCases := []struct {
//		gremlinQuery        string
//		nativeQuery         *string
//		sourceQuery         *string
//		remoteQuery         *string
//		RemoteMergeStrategy Strategy
//	}{
//		//{ // Case 00: blank
//		//	gremlinQuery: "",
//		//	nativeQuery:  utils.ToStringPtr(""),
//		//	sourceQuery:  utils.ToStringPtr(""),
//		//	remoteQuery:  utils.ToStringPtr(""),
//		//},
//		{ // Case 01: out
//			gremlinQuery:        "g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02').out('all','IsAssociatedWith-03').has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05')",
//			nativeQuery:         utils.ToStringPtr("g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02').out('all','IsAssociatedWith-03').has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05')"),
//			sourceQuery:         utils.ToStringPtr("g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02')"),
//			remoteQuery:         utils.ToStringPtr("has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05')"),
//			RemoteMergeStrategy: Strategy(APPEND),
//		},
//		{ // Case 02: filter out
//			gremlinQuery:        "g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02').filter(out('all','IsAssociatedWith-03').has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05'))",
//			nativeQuery:         utils.ToStringPtr("g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02').filter(out('all','IsAssociatedWith-03').has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05'))"),
//			sourceQuery:         utils.ToStringPtr("g.V().has('entityType','AWS.EC2.VpcPeeringConnection-01').has('property.StatusCode','active-02')"),
//			remoteQuery:         utils.ToStringPtr("has('entityType','AWS.EC2.VPC-04').in('IsAssociatedWith-05')"),
//			RemoteMergeStrategy: Strategy(FILTER),
//		},
//	}
//
//	if 1 == 1 {
//		log.Println("===============  ParserRemoteQuery")
//		for i, tc := range testCases {
//			log.Println("AAAAAAA===============", i)
//			log.Println("-- ", tc.gremlinQuery)
//
//			for i, tc := range testCases {
//				parser := &RemoteConnectionParser{}
//				inputStream := antlr.NewInputStream(tc.gremlinQuery)
//				lexer := gremlin.NewgremlinLexer(inputStream)
//				stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
//
//				gParser := gremlin.NewgremlinParser(stream)
//				gParser.BuildParseTrees = true
//
//				tree := gParser.Query()
//
//				antlr.NewParseTreeWalker().Walk(parser, tree)
//
//				if parser.query == nil {
//					t.Error("expected parser to have query initialized")
//					return
//				}
//
//				if !reflect.DeepEqual(parser.query.NativeQuery, tc.nativeQuery) {
//					t.Error("native query is not as expected", i+1, parser.nativeQuery.String())
//					return
//				}
//
//				// Source Query
//				p := parser.query.RemoteQuery.SourcePartitionQuery
//				pp := *tc.sourceQuery
//				if !reflect.DeepEqual(p, pp) {
//					t.Error("source query is not as expected", i+1, parser.sourceQuery.String())
//					return
//				}
//
//				// Remote Query
//				p = parser.query.RemoteQuery.RemotePartitionQueries[0].RemotePartitionQuery
//				pp = *tc.remoteQuery
//				println("p = ", p)
//				println("pp = ", pp)
//				if !reflect.DeepEqual(p, pp) {
//					t.Error("remote query is not as expected", i+1, parser.remoteQuery.String())
//					return
//				}
//
//				// RemoteMergeStrategy  - 	RemoteRelationshipFilters
//				log.Println("RemoteMergeStrategy  - RemoteRelationshipFilters")
//				f := parser.query.RemoteQuery.RemoteMergeStrategy
//				ff := tc.RemoteMergeStrategy
//				println("f = ", f)
//				println("ff = ", ff)
//				if !reflect.DeepEqual(f, ff) {
//					t.Error("RemoteMergeStrategy is not as expected", i+1, f)
//					return
//				}
//				log.Println("====  ending test")
//			}
//			log.Println("BBBBB===============", i)
//		}
//	}
//}
