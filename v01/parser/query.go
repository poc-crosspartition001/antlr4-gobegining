package parser

type Strategy string

const (
	APPEND Strategy = "append"
	TREE   Strategy = "tree"
	FILTER Strategy = "filter"
)

type Direction string

const (
	OUT  Direction = "out"
	IN   Direction = "in"
	BOTH Direction = "both"
)

// Query parser generates the query structure for a given gremlin query by using antlr4 parsing tto be used by query engine
// to execute the query.
type Query struct {
	// Native query to be completely executed in source partition.This is an optional field, and will be populated
	// if traversal edge scope is specified as ALL.
	NativeQuery *string

	// Remote query represents the components of the gremlin query to execute in source and remote partition
	RemoteQuery RemoteQuery

	// Merge strategy dictates the strategy to combine results from Native and Remote Query
	MergeStrategy Strategy
}

type RemoteQuery struct {
	// Indicates section of the gremlin query to be executed in source partition
	SourcePartitionQuery string

	// Indicates gremlin queries to be executed in remote partitions identified by remote relationships
	RemotePartitionQueries []RemotePartitionQuery

	// Terminal construct to be applied to source and remote queries
	TerminalConstruct string

	// The strategy to combine results from all remote partition queries
	RemoteMergeStrategy Strategy

	// The strategy to combine results from source and remote partition query
	SourceRemoteMergeStrategy Strategy
}

type RemotePartitionQuery struct {
	// Key identifier that attaches source section of the query to the corresponding remote query
	SourcePartitionQueryResponseKey string

	// Direction of remote relationship - Out/In/Both
	RemoteRelationshipDirection Direction

	// Indicates the response of remote traversal step - OutE/InE/BothE (edge response) vs Out/In/Both (vertex response)
	IsReturnTypeEdge bool

	// Remote relationship labels
	RemoteRelationshipLabels []string

	// Represents the filter criteria on relationships (if any specified). This is applicable to steps OutE/InE/BothE.
	RemoteRelationshipFilters Filter

	// Indicates the gremlin query to be executed in remote partition.
	RemotePartitionQuery string
}

type Filter struct {
	// Filter criteria for remote relationships.
	query string

	// TODO: For starters, we will have the criteria defined as gremlin query, but this will ideally be transformed to
	//  ES filter query that can be directly used by query engine for filtering remote relationships.
}
